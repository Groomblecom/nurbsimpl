use plotters::prelude::*;

use nurbsimpl::{BezierCurve, BSpline, ParametricCurve, Concatenable,get_one_arc};

use ttf_parser as ttf;

extern crate nalgebra as na;

const POINT_SIZE: i32 = 10;

use log::{Level, trace, debug};

// Native dependencies:
#[cfg(not(target_arch = "wasm32"))]
use std::path;

// Web dependencies
#[cfg(target_arch = "wasm32")]
use std::cell::RefCell;
#[cfg(target_arch = "wasm32")]
use wasm_bindgen::{prelude::*, JsCast};
#[cfg(target_arch = "wasm32")]
use wasm_bindgen_futures::JsFuture;
#[cfg(target_arch = "wasm32")]
use web_sys::{Request, RequestInit, Response};
#[cfg(target_arch = "wasm32")]
use js_sys::Uint8Array;

#[cfg(target_arch = "wasm32")]
use plotters_canvas::CanvasBackend;

type TransformFunction = Box<dyn Fn((i32, i32)) -> Option<(f32, f32)>>;

#[cfg(target_arch = "wasm32")]
struct InteractiveState<C: ParametricCurve<f32, na::U2>> {
    curve: C,
    point: Option<(usize, na::Point2<f32>)>,
    transform: TransformFunction,
}

#[cfg(target_arch = "wasm32")]
trait InteractiveStateT {
    fn select_control_point(&mut self, x: i32, y: i32);
    fn drag_control_point(&mut self, x: i32, y: i32);
    fn reset_control_point(&mut self);
    fn set_transform
        (&mut self, t: TransformFunction);
}

// Current plot code to support modifying control points interactively
#[cfg(target_arch = "wasm32")]
thread_local! {
    static CUR_CURVE: RefCell<Option<Box<dyn InteractiveStateT>>> = 
        RefCell::new(None);
}

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub async fn make_plot(which: usize) {
    make_plot_maybe(which).await.unwrap();
}

#[cfg_attr(target_arch = "wasm32", wasm_bindgen)]
pub fn num_plots_available() -> usize {
    17
}

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub fn start() {
    console_log::init_with_level(Level::Trace).unwrap();
    trace!("Hello from NURBSImpl!");
}

pub async fn make_plot_maybe(which: usize) -> Result<(), Box<dyn std::error::Error>> {
    use na::Vector2 as v2;
    match which {
        1 => {
            let pts = na::Matrix2::new(
                1.0, 2.0,
                1.0, 2.5);
            let curve = BezierCurve::new(pts.clone());
            let ipts: Vec<na::Vector2<f32>> = pts.column_iter().map(|x| {
                x.into_owned()
            }).collect();
            plot_curve("simple", &curve, &ipts)?;
        },
        2 => {
            let pts = na::Matrix2x3::new(
                0.0, 1.0, 2.0,
                0.0, 1.0, 0.0);
            let curve = BezierCurve::new(pts.clone());
            let ipts: Vec<na::Vector2<f32>> = pts.column_iter().map(|x| {
                x.into_owned()
            }).collect();
            plot_curve("less-simple", &curve, &ipts)?;
        },
        3 => {
            let mut coords = vec![];
            for theta in 0..=6 {
                let theta = theta as f32/3.0*std::f32::consts::PI;
                coords.push(f32::cos(theta));
                coords.push(f32::sin(theta));
            }
            let pts = na::Matrix::<_, na::U2, na::U7, _>::from_iterator(coords.drain(..));
            let curve = BezierCurve::new(pts.clone());
            let ipts: Vec<na::Vector2<f32>> = pts.column_iter().map(|x| {
                x.into_owned()
            }).collect();
            plot_curve("circle", &curve, &ipts)?; 
        },
        5|6 => {
            let k = vec![0.0f32,0.0,0.0,0.0,3.0/10.0,7.0/10.0,1.0,1.0,1.0,1.0];
            let pts = vec![v2::new(0.0,0.0),v2::new(2.0,1.0),v2::new(1.0,3.0),
                           v2::new(4.0,4.0),v2::new(3.0,2.0),v2::new(5.0,0.0)];

            let curve = BSpline::<f32,na::U2,na::U4>::new(pts.clone(),k);
            if which == 5 {
                plot_curve("before-refinement", &curve, &pts)?;
            } else {
                let x = vec![3.0f32/20.0,11.0/20.0,17.0/20.0];
                let new_curve = curve.knot_refinement(&x);
                plot_curve("after_refinenment" , &new_curve,new_curve.get_control_points())
                    ?;
            }
        },
        7|8 => {
            let k = vec![0.0f32,0.0,1.0/2.0,1.0,1.0];
            let pts = vec![v2::new(0.0,0.0),v2::new(2.0,2.0),v2::new(4.0,0.0)];
            let curve = BSpline::<_,_,na::U2>::new(pts,k);
            let x = vec![3.0f32/20.0,11.0/20.0,17.0/20.0];
            if which == 7 {
                plot_curve("Before_refinement_first_order" , &curve,curve.get_control_points())?;
            } else {
                let new_curve = curve.knot_refinement(&x);
                plot_curve("After_refinement_first_order" , &new_curve,new_curve.get_control_points())?;
            }
        },
        9|10 => {
            let k = vec![0.0f32,0.0,0.0,0.0,1.0,1.0,1.0,1.0];
            let pts = vec![v2::new(0.0,0.0),v2::new(1.0,2.0),v2::new(4.0,3.0),v2::new(5.0,0.0)];
            let curve = BSpline::<_,_,na::U4>::new(pts,k);
            let x = vec![3.0f32/20.0,11.0/20.0,17.0/20.0];
            if which == 9 {
                plot_curve("Before_refinement_Curve" , &curve,curve.get_control_points())?;
            } else {
                let new_curve = curve.knot_refinement(&x);
                plot_curve("After_refinement_Curve" , &new_curve,new_curve.get_control_points())?;
            }
        },
        4 => {
            let ttf_path = "assets/Italianno-Regular.ttf";
            #[cfg(target_arch = "wasm32")]
            let font_data = web_read_binary(ttf_path).await.unwrap();

            #[cfg(not(target_arch = "wasm32"))]
            let font_data = std::fs::read(ttf_path)?;

            plot_font(&font_data, ttf_path, "NURBSimpl")?;

        },
        11|12|13 => {
            let a = na::Vector2::<f32>::new(0.0, 0.0);
            let b = na::Vector2::<f32>::new(1.0, 1.0);
            let c = na::Vector2::<f32>::new(2.0, 0.0);
            let d = na::Vector2::<f32>::new(3.0,-1.0);
            let e = na::Vector2::<f32>::new(4.0, 0.0);
            let first = BSpline::<_, _, na::U3>::new(vec![a, b, c], 
                                                     vec![0.0, 0.0, 0.0, 1.0, 1.0, 1.0]);
            let second = BSpline::<_, _, na::U3>::new(vec![c, d, e], 
                                                     vec![0.0, 0.0, 0.0, 1.0, 1.0, 1.0]);
            let concat = first.concat(&second).unwrap();
            match which {
                11 => plot_curve("first segment", &first, first.get_control_points())?,
                12 => plot_curve("second segment", &second, second.get_control_points())?,
                13 => plot_curve("concatenated curve", &concat, concat.get_control_points())?,
                _ => unreachable!(),
            };
        },
        14|15 => {
                	
            let k = vec![0.0f32, 0.0, 0.0, 0.0, 1.0/5.0, 2.0/5.0, 3.0/5.0, 4.0/5.0, 1.0, 1.0, 1.0, 1.0];
	        let k_len = k.len()+1;
	        let pts = vec![v2::new(-1.0,-1.0),v2::new(1.0,3.0),v2::new(2.0,1.0),
	        v2::new(3.0,2.0),v2::new(4.0,4.0),v2::new(5.0,-1.0),v2::new(5.0,-2.0),v2::new(5.0,0.0)];
	        let curve = BSpline::<_,_,na::U4>::new(pts,k);
	        let u = 3.5/5.0;
	        let knot_index = 6;
	        let multiplicity = 0;
	        let subdivisions = 1;
	        println!("before: {:?} ",curve);
	        let new_curve = curve.insert_knot(u, knot_index, multiplicity, subdivisions);
	        println!("after: {:?} ",new_curve);
	        if which == 14 {
	            plot_curve("before_insertion" , &curve,curve.get_control_points())?;    
    	    }else{
    	        plot_curve("after_insertion" , &new_curve,new_curve.get_control_points())?;
    	    }
    	},
    	16 =>{
    	use na::Vector3 as v3;
    	let point0 = v3::new(0.0,0.0,0.0);
    	let tangent0 = v3::new(0.0,-1.0,0.0);
    	let point2 = v3::new(4.0,0.0,0.0);
    	let tangent2 = v3::new(4.0,-1.0,0.0);
	let point_on_arc = v3::new(2.0,2.0,0.0);
	let new_curve = get_one_arc(tangent0,tangent2,point0,point2,point_on_arc);
	
    	 plot_curve("Arc" , &new_curve,new_curve.get_control_points())?;
    	},
        _ => {
            panic!("Unknown plot number.");
        }
    }
    Ok(())
}

#[cfg(target_arch = "wasm32")]
async fn web_read_binary(path: &str) -> Result<Vec<u8>, JsValue> {
    // fetch the font file
    let mut opts = RequestInit::new();
    opts.method("GET");
    let request = Request::new_with_str_and_init(path, &opts)?;
    let window = web_sys::window().unwrap();
    let resp_raw = JsFuture::from(window.fetch_with_request(&request)).await?;
    let resp: Response = resp_raw.dyn_into().unwrap();
    let resp_buf_raw = JsFuture::from(resp.array_buffer()?).await?;
    let resp_buf_uint8 = Uint8Array::new_with_byte_offset(&resp_buf_raw, 0);
    Ok(resp_buf_uint8.to_vec())
}

enum LowOrderCurve {
    First(na::Matrix2<f32>),
    Second(na::Matrix2x3<f32>),
    Third(na::Matrix2x4<f32>),
}

impl LowOrderCurve {
    fn get_control_pts(&self, into: &mut Vec<(usize, na::Vector2<f32>)>) {
        use crate::LowOrderCurve::*;
        match self {
            First(c) => {
                into.extend(c.column_iter().map(|x| {
                    x.into_owned()
                }).enumerate());
            },
            Second(c) => {
                into.extend(c.column_iter().map(|x| {
                    x.into_owned()
                }).enumerate());
            },
            Third(c) => {
                into.extend(c.column_iter().map(|x| {
                    x.into_owned()
                }).enumerate());
            },
        }
    }
    fn plot(self, into: &mut Vec<(f32, f32)>) {
        use crate::LowOrderCurve::*;
        match self {
            First(c) => {
                let curve = BezierCurve::new(c);
                for u in 0..=200 {
                    let u = u as f32/200.0;
                    let pt = curve.eval_at(u);
                    into.push((pt.x, pt.y));
                }
            },
            Second(c) => {
                let curve = BezierCurve::new(c);
                for u in 0..=200 {
                    let u = u as f32/200.0;
                    let pt = curve.eval_at(u);
                    into.push((pt.x, pt.y));
                }
            },
            Third(c) => {
                let curve = BezierCurve::new(c);
                for u in 0..=200 {
                    let u = u as f32/200.0;
                    let pt = curve.eval_at(u);
                    into.push((pt.x, pt.y));
                }
            },
        }
    }
}

struct CurveBuilder {
    curves: Vec<LowOrderCurve>,
    offset: na::Vector2<f32>,
    current_loc: Option<na::Vector2<f32>>,
}

impl CurveBuilder {
    fn offset(&mut self, x: f32, y: f32) {
        self.offset += na::Vector2::new(x, y);
    }
}

fn tuple_to_vec2(t: (f32, f32)) -> na::Vector2<f32> {
    return na::Vector2::new(t.0, t.1);
}

impl ttf::OutlineBuilder for CurveBuilder {
    fn move_to(&mut self, x: f32, y: f32) {
        self.current_loc = Some( tuple_to_vec2((x, y)));
    }
    fn line_to(&mut self, x: f32, y: f32) {
        if let Some(loc) = self.current_loc {
            let mut pts = na::Matrix2::new(
                loc.x, x,
                loc.y, y);
            for mut col in pts.column_iter_mut() {
                col += self.offset;
            }
            self.curves.push(LowOrderCurve::First(pts));
        } else {
            panic!("Must have a current location before calling line_to");
        }
        self.current_loc = Some( tuple_to_vec2((x, y)));
    }
    fn quad_to(&mut self, x1: f32, y1: f32, x: f32, y: f32) {
        if let Some(loc) = self.current_loc {
            let mut pts = na::Matrix2x3::new(
                loc.x, x1, x,
                loc.y, y1, y);
            for mut col in pts.column_iter_mut() {
                col += self.offset;
            }
            self.curves.push(LowOrderCurve::Second(pts));
        } else {
            panic!("Must have a current location before calling quad_to");
        }
        self.current_loc = Some( tuple_to_vec2((x, y)));
    }
    fn curve_to(&mut self, x1: f32, y1: f32, x2: f32, y2: f32, x: f32, y: f32) {
        if let Some(loc) = self.current_loc {
            let mut pts = na::Matrix2x4::new(
                loc.x, x2, x1, x,
                loc.y, y2, y1, y);
            for mut col in pts.column_iter_mut() {
                col += self.offset;
            }
            self.curves.push(LowOrderCurve::Third(pts));
        } else {
            panic!("Must have a current location before calling curve_to");
        }
        self.current_loc = Some( tuple_to_vec2((x, y)));
    }
    fn close(&mut self) {
        self.current_loc = None;
    }
}

fn plot_font(ttf_data: &[u8], title: &str, text: &str) -> Result<(), Box<dyn std::error::Error>>  {
    let face = ttf::Face::from_slice(ttf_data, 0)?;

    let mut curve_builder = CurveBuilder {
        curves: vec![], 
        offset: na::Vector2::zeros(),
        current_loc: None,
    };

    let mut height = 0.0;
    let mut depth = 0.0;
    for c in text.chars() {
        let id = face.glyph_index(c).expect("Used missing glyph!");
        let bb = face.outline_glyph(id, &mut curve_builder)
            .expect("Used missing glyph!");
        height = f32::max(bb.y_max.into(), height);
        depth = f32::min(bb.y_min.into(), depth);
        if let Some(adv) = face.glyph_hor_advance(id) {
            curve_builder.offset(adv as f32, 0.0);
        }
    }

    // Plot the entire thing
    #[cfg(target_arch = "wasm32")]
    let root = {
        let backend = CanvasBackend::new("plot-canvas")
            .expect("cannot find canvas");
        backend.into_drawing_area()
    };

    #[cfg(not(target_arch = "wasm32"))]
    let root = {
        let name = path::Path::new(title).file_name().unwrap();
        let output_path = path::Path::new("output").join(name).with_extension("png");
        BitMapBackend::new(Box::leak(Box::new(output_path)), (1920, 1080)).into_drawing_area()
    };

    root.fill(&WHITE)?;
    let root = root.margin(10, 10, 10, 10);
    // After this point, we should be able to draw construct a chart context
    let mut chart = ChartBuilder::on(&root)
        // Set the caption of the chart
        .caption(title, ("sans-serif", 40).into_font())
        // Set the size of the label region
        .x_label_area_size(20)
        .y_label_area_size(40)
        // Finally attach a coordinate on the drawing area and make a chart context
        .build_cartesian_2d(-1f32..(curve_builder.offset.x + 200.0), depth..height)?;

    // Then we can draw a mesh
    chart
        .configure_mesh()
        // We can customize the maximum number of labels allowed for each axis
        .x_labels(5)
        .y_labels(5)
        .draw()?;

    let mut ipts = vec![];
    let mut smpts = vec![];
    for curve in curve_builder.curves {
        curve.get_control_pts(&mut ipts);
        curve.plot(&mut smpts);
    }

    // And we can draw something in the drawing area
    chart.draw_series(LineSeries::new(
        smpts,
        &RED,
    ))?;
    // Similarly, we can draw point series
    chart.draw_series(PointSeries::of_element(
        ipts,
        5,
        &RED,
        &|c, s, st| {
            return EmptyElement::at((c.1.x, c.1.y))    // We want to construct a composed element on-the-fly
            + Circle::new((0,0),s,st.filled()) // At this point, the new pixel coordinate is established
        },
    ))?;

    Ok(())
}
   
fn plot_curve<C>(sname: &str,
                 curve: &C,
                 control_points: &Vec<na::Vector2<f32>>
                 )
    -> Result<Option<TransformFunction>, Box<dyn std::error::Error>> 
    where
    C: ParametricCurve<f32, na::U2> + Clone + 'static,
    na::DefaultAllocator: na::allocator::Allocator<f32, na::U2>
{
    // Compute bounding box (by convex hull property)
    let (mut min_x, mut min_y, mut max_x, mut max_y) = (1e4f32, 1e4, -1e4, -1e4);
    use std::cmp::Ordering;
    for point in control_points {
        match (point.x.partial_cmp(&min_x), point.x.partial_cmp(&max_x)) {
            (Some(Ordering::Less), _) => {min_x = point.x}
            (_, Some(Ordering::Greater)) => {max_x = point.x}
            _ => {}
        }
        match (point.y.partial_cmp(&min_y), point.y.partial_cmp(&max_y)) {
            (Some(Ordering::Less), _) => {min_y = point.y}
            (_, Some(Ordering::Greater)) => {max_y = point.y}
            _ => {}
        }
    }
    #[cfg(target_arch = "wasm32")]
    let root = {
        let backend = CanvasBackend::new("plot-canvas")
            .expect("cannot find canvas");
        backend.into_drawing_area()
    };
    #[cfg(not(target_arch = "wasm32"))]
    let root = {
        let name = format!("output/plot-{}.png", sname);
        // you can deal with leaking ~20 bytes of memory to work around the stupid API design here,
        // ok
        BitMapBackend::new(Box::leak(Box::new(name)), (1920, 1080)).into_drawing_area()
    };

    root.fill(&WHITE)?;
    let root = root.margin(10, 10, 10, 10);
    // After this point, we should be able to draw construct a chart context
    let mut chart = ChartBuilder::on(&root)
        // Set the caption of the chart
        .caption(sname, ("sans-serif", 40).into_font())
        // Set the size of the label region
        .x_label_area_size(20)
        .y_label_area_size(40)
        // Finally attach a coordinate on the drawing area and make a chart context
        .build_cartesian_2d(min_x..max_x, min_y..max_y)?;

    // Then we can draw a mesh
    chart
        .configure_mesh()
        // We can customize the maximum number of labels allowed for each axis
        .x_labels(5)
        .y_labels(5)
        .draw()?;

    let mut smpts = vec![];
    for u in 0..=200 {
        let u = u as f32/200.0;
        let pt = curve.eval_at(u);
        smpts.push((pt.x, pt.y));
    }
    // And we can draw something in the drawing area
    chart.draw_series(LineSeries::new(
        smpts,
        &RED,
    ))?;
    let ipts: Vec<(usize, na::Vector2<f32>)> = control_points.iter().map(|x| {
        x.into_owned()
    }).enumerate().collect();
    // Similarly, we can draw point series
    chart.draw_series(PointSeries::of_element(
        ipts,
        POINT_SIZE,
        &RED,
        &|c, s, st| {
            return EmptyElement::at((c.1.x, c.1.y))    // We want to construct a composed element on-the-fly
            + Circle::new((0,0),s,st.filled()) // At this point, the new pixel coordinate is established
            + Text::new(format!("P{} ({:.2}, {:.2})", c.0, c.1.x, c.1.y), (10, 0), ("sans-serif", 10).into_font());
        },
    ))?;
    // Stash the curve in the thread-local so that interactive mode demos can use it (only used
    // from web)
    let mut chart_transform = Some(chart.into_coord_trans());
    #[cfg(target_arch = "wasm32")]
    CUR_CURVE.with(|c| {
        let b = c.try_borrow_mut();
        if let Ok(mut c) = b {
            let state = InteractiveState{
                curve: (*curve).clone(),
                point: None,
                transform: Box::new(chart_transform.take().unwrap()),
            };
            *c = Some(Box::new(state));
        }
    });
    if let Some(t) = chart_transform {
        Ok(Some(Box::new(t)))
    } else {
        Ok(None)
    }
}

#[cfg(target_arch = "wasm32")]
impl<C> InteractiveStateT for InteractiveState<C> where
C: ParametricCurve<f32, na::U2> + Clone + 'static
{
    fn select_control_point(&mut self, x: i32, y: i32) {
        trace!("mouse orig coords: {}, {}", x, y);
        // shhhhhh let's just hope the function is linear
        // spoiler: it is for these plots, anyhow. that said --
        // don't reuse this for polar stuff.
        let (ab, cd) = ((x, y), (x+POINT_SIZE, y));
        let (x, y, point_size) = 
            match ((self.transform)(ab), (self.transform)(cd)) {
                (Some((a, b)), Some((c, _d))) => (a, b, c-a),
                _ => return,
            };
        trace!("mouse transformed coords: {}, {}", x, y);
        trace!("Point radius transformed: {}", point_size);
        let mouse_point = na::Point2::new(x, y);
        // Loop over control points to find the one
        for (i, &pt) in self.curve.get_control_points().iter().enumerate() {
            // find if within radius
            let c_point = na::Point2::new(pt.x, pt.y);
            if na::distance(&mouse_point, &c_point) <= point_size*2.0 {
                self.point = Some((i, c_point));
                break;
            }
            
        }
    }
    fn drag_control_point(&mut self, x: i32, y: i32) {
        let (x, y) = match (self.transform)((x, y)) {
            Some(a) => a,
            None => return,
        };
        let mouse_point = na::Point2::new(x, y);
        if let Some((which, c_point)) = self.point {
            let diff = mouse_point - c_point;
            self.curve.shift_control_point(which, &diff);
            self.point = Some((which, mouse_point));
        }
        let control_points = self.curve.get_control_points().into_owned();
        let res = plot_curve("Modified curve", &self.curve, &control_points)
            .unwrap();
        if let Some(res) = res {
            self.transform = res;
        }
    }
    fn set_transform
        (&mut self, t: Box<dyn Fn((i32, i32)) -> Option<(f32, f32)>>) {
            self.transform = t;
        }
    fn reset_control_point(&mut self) {
            self.point = None;
        }
}

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub fn select_control_point(x: i32, y: i32) {
    CUR_CURVE.with(|c| {
        trace!("Selecting control point");
        if let Some(ref mut state) = *c.borrow_mut() {
            trace!("There is some state!");
            state.select_control_point(x, y);
        } else {
            debug!("Ignoring select attempt");
        }
    });
}

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub fn drag_control_point(x: i32, y: i32) {
    CUR_CURVE.with(|c| {
        if let Some(ref mut state) = *c.borrow_mut() {
            state.drag_control_point(x, y);
        } else {
            debug!("Ignoring drag attempt");
        }
    });
}

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub fn reset_control_point() {
    CUR_CURVE.with(|c| {
        if let Some(ref mut state) = *c.borrow_mut() {
            state.reset_control_point();
        } else {
            debug!("Ignoring reset attempt");
        }
    });
}
