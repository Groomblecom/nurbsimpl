#[cfg(test)]
mod test;

/// Data structures and operations for topological-level operations
pub mod topo;

extern crate nalgebra as na;

use nalgebra::{Dim, DimAdd, DimName, DimNameAdd, DimNameSum, DimSum, U1, U3, RealField, DimNameMul};

use num_integer::binomial;

use approx::ulps_eq;

use std::marker::PhantomData;
use std::borrow::Cow;
use std::ops::{AddAssign, SubAssign, Sub};

use core::fmt::Debug;
use std::fmt;
use std::error::Error;

/// Error type for when two points should be the same point, but are not.
#[derive(Debug, Clone)]
pub struct MismatchedPoints<N>
where
N: RealField,
{
    first: Option<na::DVector<N>>,
    second: Option<na::DVector<N>>,
    operation: &'static str,
}

impl<N> fmt::Display for MismatchedPoints<N>
where
N: RealField,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}. However, the points were actually {:?} and {:?}", 
               self.operation, self.first, self.second)
    }
}

impl<N> Error for MismatchedPoints<N> 
where
N: RealField,
{}

/// Abstracts over curves that take a scalar parameter to a vector.
///
/// Unless noted otherwise, parameters are taken to be in [0, 1].
pub trait ParametricCurve<N, D>: Debug
where
D: DimName,
N: RealField,
na::DefaultAllocator: na::allocator::Allocator<N, D>,
{
    /// Returns the curve's value at the specified parameter.
    fn eval_at(&self, u: N) -> na::OVector<N, D>;
    /// Return knots for this curve.
    ///
    /// A knot is a parameter value where the specific control points that influence the curve
    /// change. That is to say, it is the values where a piecewise function changes pieces. This
    /// function returns the _unique_ knots; this is not the raw knot vector for a BSpline.
    fn get_knots(&self) -> Cow<[N]>;
    /// Return knots and their multiplicities for this curve.
    ///
    /// A knot is a parameter value where the specific control points that influence the curve
    /// change. That is to say, it is the values where a piecewise function changes pieces. This
    /// function returns the _unique_ knots and their counts; this could represent the raw knot
    /// vector for a BSpline.
    fn get_knots_with_mult(&self) -> Cow<[(N, usize)]>;
    /// Return control points for this curve
    fn get_control_points(&self) -> Cow<[na::OVector<N, D>]>;
    /// Shift a particular control point over by some amount.
    fn shift_control_point(&mut self, which: usize, by: &na::OVector<N, D>);
    /// Provides a bounding box via strong convex hull property, via the corners
    fn bounding_box(&self) -> (na::OVector<N, D>, na::OVector<N, D>) {
        let mut control_points = self.get_control_points().into_owned();
        let mut lower = control_points.pop().unwrap();
        let mut upper = lower.clone();
        for pt in control_points {
            for i in 0..D::dim() {
                if lower[i] > pt[i] {
                    lower[i] = pt[i];
                } else if upper[i] < pt[i] {
                    upper[i] = pt[i];
                }
            }
        }
        (lower, upper)
    }
}

/// Abstracts over pairs of curves that may be concatenated (joined at a point)
pub trait Concatenable<N, D, C>: ParametricCurve<N, D>
where
C: ParametricCurve<N, D>,
N: RealField,
D: DimName,
na::DefaultAllocator: na::allocator::Allocator<N, D>,
{
    /// Resulting curve type from this concat (since it may have to promote one
    /// or both curves to a more powerful representation)
    type ResultingCurve: ParametricCurve<N, D>;

    /// Compute the curve that results from traversing this curve first, then 
    /// the other curve.
    fn concat(&self, other: &C) ->
        Result<Self::ResultingCurve, MismatchedPoints<N>>;
}

/// Abstracts over curves that have derivatives.
///
/// For now, this is only implemented for BSplines and NURBS, even though Bezier curves, as a
/// subset of BSplines, do have perfectly good derivatives. This is due to time constraints -- just
/// promote your Bezier to a BSpline (or contribute a Bezier derivative algorithm!)
pub trait DifferentiableCurve<N, D, O>: ParametricCurve<N, D>
where
O: DimName + DimNameAdd<U1> + na::DimSub<U1>,
<O as na::DimSub<U1>>::Output: DimName,
N: RealField,
D: DimName +  DimNameAdd<U1>,
na::DefaultAllocator: na::allocator::Allocator<N, D> 
+ na::allocator::Allocator<N, U1, O>
+ na::allocator::Allocator<N, <D as DimNameAdd<U1>>::Output>,
{
    /// Gives the value of the curve and its derivatives at a specific parameter value.
    ///
    /// The columns of the resulting matrix are the derivative vectors. The zeroeth column has the
    /// zeroth derivative (i.e. the curve's value).
    fn eval_with_derivatives_at<ND>(&self, u: N, num_derivatives: ND) -> na::OMatrix<N, D, ND>
        where
            ND: Dim + na::DimMin<na::DimDiff<O, U1>>,
            na::DefaultAllocator: na::allocator::Allocator<N, D, ND>
            + na::allocator::Allocator<N, na::DimMinimum<ND, na::DimDiff<O, U1>>, O>
            + na::allocator::Allocator<N, ND, O>
            + na::allocator::Allocator<N, <D as DimNameAdd<U1>>::Output, ND>
            ;
    /// Finds a parameter value nearest to given point, and the distance at that point.
    ///
    /// This function must use sucessive approximation and is not fast. The tuple is (Parameter,
    /// Distance)
    /// 
    /// Boundary controls whether the curve is a loop or not.
    fn point_inversion(&self, point: &na::OVector<N, D>, boundary: bool) -> (N, N) 
    where
            U3: Dim + na::DimMin<na::DimDiff<O, U1>>,
            na::DefaultAllocator: na::allocator::Allocator<N, D, U3>
            + na::allocator::Allocator<N, na::DimMinimum<U3, na::DimDiff<O, U1>>, O>
            + na::allocator::Allocator<N, U3, O>
            + na::allocator::Allocator<N, <D as DimNameAdd<U1>>::Output, U3>
    {
        // Snag 3 points of every knot span for newton approx
        let uis = self.get_knots();
        let mut best_param = N::zero();
        let mut best_dist = point.metric_distance(&self.eval_at(best_param));
        let two = N::one()+N::one();
        let three = two+N::one();
        let five = two+three;
        for pair in uis.windows(2) {
            for i in &[N::one()/five, two/five, three/five] {
                let ashare = *i;
                let bshare = N::one() - *i;
                let u = pair[0] * ashare + pair[1] * bshare;
                let dist = point.metric_distance(&self.eval_at(u));
                if dist < best_dist {
                    best_dist = dist;
                    best_param = u;
                }
            }
        }
        // now we start newton iteration. How many cycles? until we find a point on the curve, find
        // a point such that the normal vector of the curve points right at it, or it stops getting
        // better. Hold onto your CPUs.
        loop {
            // get zeroth, first, and second derivative
            let c = self.eval_with_derivatives_at(best_param, na::U3::name());
            // Has the point converged?
            let difference = c.column(0).sub(point);
            best_dist = difference.norm();
            if ulps_eq!(best_dist, N::zero()) {
                break;
            }
            let dot = c.column(1).dot(&difference);
            let dot_normed = dot/c.column(1).norm()/difference.norm();
            if ulps_eq!(dot_normed, N::zero()) {
                break;
            }
            // not doing it? Ok, compute the next step
            let denom = c.column(2).dot(&difference) + c.column(1).norm_squared();
            let old_param = best_param;
            best_param -= dot/denom;
            if boundary {
                // Clamp back onto the curve
                if best_param < N::zero() {
                    best_param = N::zero();
                }
                if best_param > N::one() {
                    best_param = N::one();
                }
            } else {
                // Clamp back onto the curve
                if best_param < N::zero() {
                    best_param = best_param + N::ceil(-best_param);
                }
                if best_param > N::one() {
                    best_param = best_param - N::floor(best_param);
                }
            }
            // Did we actually make a decent-size change?
            if ulps_eq!((best_param - old_param)*c.column(1).norm(), N::zero()) {
                break;
            }
        }
        (best_param, best_dist)
    }
}


/// Non-Uniform Rational B-Spline
#[derive(Debug, Clone)]
pub struct NURBS<N, D, O> where
N: RealField,
D: DimName + DimNameAdd<U1> + Dim + DimAdd<U1>,
O: DimName,
na::DefaultAllocator: na::allocator::Allocator<N, DimNameSum<D, U1>>,
DimNameSum<D, U1>: DimNameMul<U1>,
{
    bspline: BSpline<N, DimNameSum<D, U1>, O>,
}

/// (nonrational) B-Spline
///
/// N is the type of the scalar field associated with the vector space used to
/// represent the points. D is the dimension of that vector space. O is the
/// order of the B-Spline. 
#[derive(Debug, Clone)]
pub struct BSpline<N, D, O> where
N: RealField,
D: DimName + DimNameMul<U1>,
O: DimName,
na::DefaultAllocator: na::allocator::Allocator<N, D>
{
    control_points: Vec<na::OVector<N, D>>,
    knot_vector: Vec<N>,
    order_phantom: PhantomData<O>,
}

#[derive(Debug, Clone)]
/// Single Bezier Curve segment
///
/// Stores all (i.e. including on-curve) points.
/// Order parameter statically determines number of control points
pub struct BezierCurve<N, D, O, S> where
N: RealField,
D: DimName,
O: DimName,
S: na::storage::Storage<N, D, O>
{
    control_points: na::Matrix<N, D, O, S>
}


impl<N, D, O> BSpline<N, D, O> where
N: RealField,
D: DimName + DimNameMul<U1>,
O: DimName,
na::DefaultAllocator: na::allocator::Allocator<N, D> + na::allocator::Allocator<N, O, U1> 
{
    /// Creates a new BSpline.
    ///
    /// The number of control points plus the order of the curve MUST equal the length of the knot
    /// vector. This is enforced at runtime by panicing.
    pub fn new(control_points: Vec<na::OVector<N, D>>, knot_vector: Vec<N>)
        -> Self {
            assert_eq!(knot_vector.len() - control_points.len(), O::dim(),
            "The difference of the knot vector length and number of {}",
            "control points must be equal to the order.");
            assert!(control_points.len() > 0, "You must have at least one control point.");
            BSpline {
                control_points, knot_vector, order_phantom: PhantomData
            }
        }
    /// Gives the value of the curve at a specific parameter value.
    pub fn eval_at(&self, u: N) -> na::OVector<N, D>
        where na::DefaultAllocator: na::allocator::Allocator<N, D, na::U1> + 
        na::allocator::Allocator<N, O, na::U1>
        {
            let span = self.find_span(u);
            let basis_functions = self.basis_functions(u, span);
            let mut c = na::OVector::<N, D>::zeros();
            for i in 0..O::dim() {
                let temp = &self.control_points[span+i-(O::dim()-1)];
                c = c + temp.scale(basis_functions[i]);
            }
            c
        }
    /// Gives the value of the curve and its derivatives at a specific parameter value.
    ///
    /// The columns of the resulting matrix are the derivative vectors. The zeroeth column has the
    /// zeroth derivative (i.e. the curve's value).
    pub fn eval_with_derivatives_at<ND, AD>(&self, u: N, num_derivatives: ND) 
        -> na::OMatrix<N, D, ND>
        where 
        ND: Dim + na::DimMin<na::DimDiff<O, U1>>,
        AD: Dim,
        O: DimNameAdd<U1> + na::DimSub<U1>,
        <O as na::DimSub<U1>>::Output: DimName,
        na::constraint::ShapeConstraint: na::constraint::DimEq<na::DimMinimum<ND, na::DimDiff<O, U1>>, AD>,
        na::DefaultAllocator: na::allocator::Allocator<N, na::Dynamic, O> 
            + na::allocator::Allocator<N, O, O>
            + na::allocator::Allocator<N, ND, O>
            + na::allocator::Allocator<N, D, ND>
            + na::allocator::Allocator<N, O, na::U1>
            + na::allocator::Allocator<N, D, na::U1>
            + na::allocator::Allocator<N, na::U1, D>
            + na::allocator::Allocator<N, na::DimMinimum<ND, na::DimDiff<O, U1>>, O>
        {
            let act_derivatives = num_derivatives.min(<O as na::DimSub<U1>>::Output::name());
            let mut out = na::OMatrix::zeros_generic(D::name(), num_derivatives);
            let span = self.find_span(u);
            let basis_functions = self.basis_functions_with_derivative(u, span, act_derivatives);
            for k in 0..act_derivatives.value() {
                for i in 0..O::dim() {
                    let temp = &self.control_points[span+i-(O::dim()-1)];
                    out.column_mut(k).add_assign(temp.scale(basis_functions[(k, i)]));
                }
            }
            out
        }
    /// Finds the upper index of the knot span for a given parameter value.
    fn find_span(&self, u: N) -> usize {
        if u == self.knot_vector[self.control_points.len()] {
            return self.control_points.len() - 1
        }
        // binary search
        let mut low = O::dim()-1;
        let mut high = self.knot_vector.len()-O::dim() + 1;
        let mut mid = (low + high)/2;
        // low mid high 9 9 10
        while u < self.knot_vector[mid] || u >= self.knot_vector[mid+1] {
            if u < self.knot_vector[mid] {
                high = mid;
            } else {
                low = mid;
            }
            mid = (low+high)/2;
        }
        mid
    }

    // Riad Section
    //Step 5: Test cases
    //Step 6: be happy
    /// Inserts new knots from passed vector
    ///
    /// This functional also interpolates control points that keep the curve approximately the same
    /// shape when done. The final curve tends to follow the control points somewhat more closely.
    pub fn knot_refinement(&self, x:&Vec<N>)
        -> BSpline<N,D,O>

        {   
            let r  = x.len() -1;
            let n = self.control_points.len();
            let m = n+O::dim();
            let a = self.find_span(x[0]);
            let b = self.find_span(x[r])+1;
            let mut ncontrol_points:Vec<na::OVector<N,D>> = vec![na::zero();n+r+1];
            let mut nknot_vector = vec![na::zero();self.knot_vector.len()+r+1];

            // Diagram of new knot array:
            // 0-----------------x.len()
            //r-------------------------
            // Diagram of input knot array:
            // 0---a---b---b+p---b+p+r---m
            //   j                    j
            //i    ----------

            for j in 0..O::dim(){
                ncontrol_points[j].copy_from(&self.control_points[j]);
            }
            for j in b-1..n {
                ncontrol_points[j+r+1].copy_from(&self.control_points[j]);
            }
            // copies 0..u_k into output
            for j in 0..=a{
                nknot_vector[j] = self.knot_vector[j];
            }
            // copies u_{k+1}+p up to number of knots into output, leaving a hole for r new
            // elements
            for j in b+O::dim()-1..m{
                nknot_vector[j+r+1] = self.knot_vector[j];
            }
            // Uppermost affected control point
            // Tracks the old control points index to copy from
            let mut i = b+O::dim()-2;
            // corresponding knot vector index for that control point
            // Tracks the new control points index to copy to
            let mut k = b+O::dim()-1+r;
            for j in (0..=r).rev() {
                // while inserted entry knot is less than ith entry AND index is not yet at bottom
                // of inserted range
                while x[j] <=self.knot_vector[i] && i > a {
                    // copy over a control point from the top of the range
                    let temp_control_point = &self.control_points[i-O::dim()];
                    ncontrol_points[k-O::dim()].copy_from(temp_control_point);
                    // and copy over the corresponding knot vector entry
                    nknot_vector[k] = self.knot_vector[i];
                    // decrease the index of the uppermost affected control point
                    k=k-1;
                    // ditto corresponding knot index
                    i = i-1;
                }
                // now we know entry knot greater than ith entry OR index at bottom of affected
                // zone.

                // copy a control point down a slot.
                ncontrol_points[k-O::dim()] = ncontrol_points[k-(O::dim()-1)].clone();
                for l in 1..O::dim(){
                    // ranges over knot vector indices affecting uppermost new control point
                    let ind = k-(O::dim()-1)+l;
                    // difference of old knot vector entry and inserted one
                    let mut alpha = nknot_vector[k+1] - x[j];
                    // no difference, adding multiplicity
                    if alpha.is_zero() {
                        // then just duplicate the control point too
                        let temp_control_point = ncontrol_points[ind].clone();
                        ncontrol_points[ind-1].copy_from(&temp_control_point);
                    } else{
                        alpha = alpha/(nknot_vector[k+l]-self.knot_vector[i -(O::dim()-1)+l]);
                        // Interpolate new control point between the old two
                        // ratio at which do this is alpha
                        let temp_control_point = ncontrol_points[ind-1].scale(alpha) 
                            + ncontrol_points[ind].scale(N::one()-alpha);
                        ncontrol_points[ind -1].copy_from(&temp_control_point);
                    }
                }
                nknot_vector[k] = x[j];
                k=k-1;
            }
            Self::new(ncontrol_points, nknot_vector)
        }

    /// Removes spesific knots as many times as permitted by inputed tolerance.
    ///
    ///Constrains on multipicity of the knot (s) : 1<= s <= Order-1.
    ///
    ///Constrains on index of the knot in the knot vector (r): r <=3*Order-2.
    pub fn knot_removal(&mut self,r:usize,num:usize,knot:N,s:usize,tol:N)
    
    {
    	let m = self.control_points.len() +O::dim();
    	let fout = (2*r-s-(O::dim()-1))/2;// First Control point out
    	let mut last = r-s;
    	let mut first = r-(O::dim()-1);
    	let mut temp :Vec<na::OVector<N,D>> = vec![na::zero();2*O::dim()-1];
    	let mut t = 0;
    	while t < num {
    		
    		let off = first-1;
    		temp[0].copy_from(&self.control_points[off]);
    		temp[last+1-off].copy_from(&self.control_points[last+1]);
    		let mut i =first;
    		let mut j = last;
    		let mut ii = i;
    		let mut jj = last-off;
    		let mut remflag =0;
    		// Compute new Control_Points place them into the temperary array
    		while (j as isize)-(i as isize) > (t as isize) {
    			let alpha_i_num = knot -self.knot_vector[i];
  			let alpha_i_denom = self.knot_vector[i+O::dim()+t] - self.knot_vector[i];
  			let alpha_i = alpha_i_num / alpha_i_denom;
  		
  			let alpha_j_nume = knot -self.knot_vector[j-t];
  			let alpha_j_denom = self.knot_vector[j+O::dim()] - self.knot_vector[j-t];
  			let alpha_j = alpha_j_nume / alpha_j_denom;
    			temp[ii] = (&self.control_points[i] -temp[ii-1].scale(N::one() - alpha_i))/alpha_i;
    			temp[jj] = (&self.control_points[j] -temp[jj+1].scale(alpha_j))/(N::one() - alpha_j);
    			i=i+1;
    			j=j-1;
    			ii=ii+1;
    			jj=jj-1;
    		}//end of while loop
    		
    		if (j as isize)-(i as isize) <(t as isize) {
    			// dwmin/1+abs(Pmax)
    			// Cheks if the new control points are with in the specified tolerance
    			if temp[ii -1].metric_distance(&temp[jj+1])<= tol 
    	               {
    				remflag =1;
    		  	}
    		 }
    		 else{
    		  	let alpha_i_num = knot -self.knot_vector[i];
  			let alpha_i_denom = self.knot_vector[i+O::dim()+t] - self.knot_vector[i];
  			let alpha_i = alpha_i_num / alpha_i_denom;
  			let intermed= temp[ii+t+1].scale(alpha_i)+temp[ii-1].scale(N::one()-alpha_i);
  			// Cheks if the new control points are with in the specified tolerance
  			if self.control_points[i].metric_distance(&intermed) <= tol	
  			{
  				remflag = 1;
    		  	}
    		}
    		
    		if remflag == 0 {
    			break;
    		}
    		else{
    		   	i = first;
    			j = last;
    			// Move the Old control points back to fill the missing ones
    			while (j as isize)-(i as isize) > (t as isize){
    				self.control_points[i].copy_from(&temp[i-off]);
    				self.control_points[j].copy_from(&temp[j-off]);
    				i = i+1;
    				j = j-1;
    			}	
    		}
    		first = first - 1;
    		last = last +1;
    		t+= 1;
    	  } //end of for loop
    	  
    	  if t == 0 {
    	  	return;
    	  }
    	  // Move the Old Knot vectors back to fill the missing ones
    	  for k in r+1..m {
    	  	self.knot_vector[k-t] = self.knot_vector[k];
    	  }
    	  let mut j = fout;
    	  let mut i=j;
    	  for k in 1..t{
    	  	if k%2 == 1{
    	  		i = i+1;
    	  	}
    	  	else{
    	  		j = j-1;
    	  	}
    	  }
    	  // Restores the true size of the control point vector and knot vectros
    	  for k in i+1..self.control_points.len() {
    	  	let temp_point = self.control_points[k].clone();
    	  	self.control_points[j].copy_from(&temp_point);
    	  	j = j+1;
    	  }
    	  for index in 0..t{
    	  	self.control_points.pop();
    	  	self.knot_vector.pop();
    	  }
    	  return;
   	}
    
    /// Fetches current control points.
    pub fn get_control_points(&self)
        -> &Vec<na::OVector<N,D>>
        {
            &self.control_points
        }

    pub fn get_control_points_mut(&mut self)
        -> &Vec<na::OVector<N,D>>
        {
            &mut self.control_points
        }

    /// Fetches current knot vector.
    ///
    /// You probably should manipulate this using knot refinement rather than array operations.
    pub fn get_knot_vector(&self)
        -> &Vec<N>
        {
            &self.knot_vector
        }

    /// Computes the nonzero basis functions and their derivatives 
    /// for the span starting at i. Derivatives up to ND will be computed.
    ///
    /// Each column corresponds to a knot and higher derivatives have higher row index (so, the
    /// function's value is in the zeroth row).
    ///
    /// Note that ND must be strictly less than O or else the function will panic due to OOB writes.
    /// ND has been left potentially-dynamic so this is on the caller to get right.
    fn basis_functions_with_derivative<ND>(&self, u: N, i: usize, num_derivatives: ND) -> na::OMatrix<N, ND, O> 
        where 
            ND: Dim,
            na::DefaultAllocator: na::allocator::Allocator<N, na::Dynamic, O> 
                + na::allocator::Allocator<N, O, O>
                + na::allocator::Allocator<N, ND, O>,
        {
            if num_derivatives.value() >= O::dim() {
                panic!("Number of derivatives MUST be strictly less than order of BSpline.\
                If you really need more derivatives, they are all zero by definition.");
            }
            let mut out = na::OMatrix::<N, ND, O>::zeros_generic(num_derivatives, O::name());
            let mut ndu: na::OMatrix<N, O, O> = na::zero();
            let mut right: na::OVector<N, O> = na::zero();
            let mut left: na::OVector<N, O> = na::zero();
            ndu[(0, 0)] = N::one();
            for j in 1..O::dim() {
                left[j] = u - self.knot_vector[i+1-j];
                right[j] = self.knot_vector[i+j] - u;
                let mut saved = N::zero();
                for r in 0..j {
                    // Lower triangle
                    ndu[(j, r)] = right[r+1]+left[j-r];
                    let temp = ndu[(r, j-1)]/ndu[(j, r)];
                    // Upper triangle
                    ndu[(r, j)] = saved + right[r+1]*temp;
                    saved = left[j-r]*temp;
                }
                ndu[(j, j)] = saved;
            }
            // Copy the zeroth derivatives (e.g. the function proper) into appropriate columns
            // TODO: make this use matrix slice methods (might impact vectorization?)
            for j in 0..O::dim() {
                out[(0, j)] = ndu[(j, O::dim()-1)];
            }
            // Compute rest of the derivatives
            //
            // For sanity sake, versus The Book, a[s1] has been renamed "from" and a[s2] "to" to
            // better reflect their usage. This is fine because we are not targeting 90s C
            // compilers. DIDN"T DO TODO -- a[s1] is now a.0, a[s2] is now a.1
            //
            // r is the basis function we are on.
            for r in 0..O::dim() {
                let first: na::OVector<N, O> = na::zero();
                let second: na::OVector<N, O> = na::zero();
                let mut a = (first, second);
                a.0[0] = N::one();
                // k indexes the derivative we are calculating. -- higher derivative index = lower
                // polynomial degree
                for k in 1..num_derivatives.value() {
                    // d accumulates the current derivative value.
                    let mut d = N::zero();
                    // TODO: refactor this to avoid any signed-size variables, since it makes
                    // everything ugly (and I'm not convinced that the unsigned version would be
                    // slower or less intuitive)
                    let rk = (r as isize) - (k as isize); // How far are we from the diagonal?
                    let pk = O::dim() - 1 - k; // How many non-zero derivatives left?
                    // We are on a higher basis function index than derivative index
                    if r >= k {
                        // since r >= k, rk must be positive so this is fine
                        a.1[0] = a.0[0]/ndu[(pk+1, rk as usize)];
                        // ditto.
                        d = a.1[0]*ndu[(rk as usize, pk)];
                    }
                    // r - k >= -1 means r-1 is not strictly greater than k
                    let j1 = if rk >= -1 {
                        1
                    } else {
                        (-rk) as usize // guaranteed to work -- see above if
                    };
                    let j2 = if (r as isize)-1 <= pk as isize {
                        k - 1
                    } else {
                        (O::dim() - 1) - r
                    };
                    // j is index in accumulating scratch vectors
                    for j in j1..=j2 {
                        a.1[j] = (a.0[j] - a.0[j-1])/ndu[(pk+1, (rk+j as isize) as usize)];
                        d += a.1[j]*ndu[((rk+j as isize) as usize, pk)];
                    }
                    if r <= pk {
                        a.1[k] = -a.0[k-1]/ndu[(pk+1, r)];
                        d += a.1[k]*ndu[(r, pk)];
                    }
                    out[(k, r)] = d;
                    // reuse prev-prev row's vector as new row scratch buffer; move row scratch
                    // buffer down to use as prev row
                    a = (a.1, a.0);
                }
            }
            // whew -- now multiply on leading coefficients
            let mut r = N::from_usize(O::dim() -1)
                .expect("You must implement from_usize on your Field type");
            for k in 1..num_derivatives.value() {
                for j in 0..O::dim() {
                    out[(k, j)] *= r;
                    r *= N::from_usize(O::dim()-1-k)
                        .expect("You must implement from_usize on your Field type");
                }
            }
            out
        }

    /// Computes the nonzero basis functions for the span starting at i.
    fn basis_functions(&self, u: N, i: usize) -> na::OVector<N, O> 
        where 
        {
            let mut out: na::OVector<N, O> = na::zero();
            let mut right: na::OVector<N, O> = na::zero();
            let mut left: na::OVector<N, O> = na::zero();
            out[0] = N::one();
            for j in 1..O::dim() {
                left[j] = u - self.knot_vector[i+1-j];
                right[j] = self.knot_vector[i+j] - u;
                let mut saved = N::zero();
                for r in 0..j {
                    let temp = out[r]/(right[r+1]+left[j-r]);
                    out[r] = saved + right[r+1]*temp;
                    saved = left[j-r]*temp;
                }
                out[j] = saved;
            }
            out
        }

    /// Inserts knots with given multiplicity into a span of the knot vector
    ///
    /// The span will be split into `subdivisions` equal pieces, using knots of multiplicity
    /// `multiplicity`. Since the continuity of a knot is given by order minus multiplicity, this
    /// allows you to pick how many derivatives you will allow to be discontinuous. Bigger
    /// multiplicity gives more abrupt changes.
    ///
    /// You may wish to get the knot index using the `find_span` function.
    pub fn insert_knot(&self,u: N,knot_index: usize,multiplicity: usize, subdivisions: usize)
        ->Self
        {
            let mp = self.control_points.len()+O::dim();
            let mut rw : Vec<na::OVector<N,D>> = vec![na::zero(); O::dim()];
            let mut ncontrol_points : Vec<na::OVector<N,D>> = 
                vec![na::zero(); self.control_points.len()+subdivisions];

            let mut uq = vec![N::zero(); subdivisions+self.knot_vector.len()];
            //Load new knot vector
            for i in 0..=knot_index {
                uq[i] = self.knot_vector[i];
            }

            for i in 1..=subdivisions {
                uq[knot_index+i] = u;
            }

            for i in knot_index+1..mp{
                uq[i+subdivisions] = self.knot_vector[i];
            }

            //Save unaltered control points
            assert!(multiplicity+subdivisions<=O::dim()-1);
            for i in 0..knot_index-(O::dim()-1) {
                ncontrol_points[i].copy_from(&self.control_points[i]);
            }

            for i in knot_index-multiplicity..self.control_points.len(){
                ncontrol_points[i+subdivisions].copy_from(&self.control_points[i]);
            }

            for i in 0..=O::dim()-1-multiplicity {
                rw[i].copy_from(&self.control_points[knot_index-(O::dim()-1)+i]);
            }
            //Insert knot subdivisions times
            let mut l = 0;
            for j in 1..=subdivisions {
                l = knot_index-O::dim()+j;
                for i in 0..=O::dim()-1-j-multiplicity{
                    let denom = self.knot_vector[i+knot_index+1]
                        -self.knot_vector[l+i];
                    let numerator = u-self.knot_vector[l+i];
                    let alpha = (u-self.knot_vector[l+i])/denom;
                    rw[i] = rw[i+1].scale(alpha) + rw[i].scale(N::one()-alpha);

                }
                ncontrol_points[l].copy_from(&rw[0]);
                ncontrol_points[knot_index+subdivisions-j-multiplicity]
                    .copy_from(&rw[O::dim()-1-j-multiplicity]);
            }

            //Load remaining control points
            for i in l+1..knot_index-multiplicity {
                ncontrol_points[i].copy_from(&rw[i-l]);
            }

            Self::new(ncontrol_points,uq)
        }
    /// concatenates a curve with parameter in [0, 1] to self with parameter in
    /// [0, param_max], resulting in a curve with parameter in [0, param_max+1]
    fn concat_unnormalized(&mut self, other: &Self, param_max: N)
        -> Result<(), MismatchedPoints<N>> {
        // Check endpoint match
        let last = self.control_points.last().map(|x| x.clone());
        let fill_pt = if last.as_ref() == other.control_points.first() && last != None {
            // Will never panic, since we can't get to this branch if last is None.
            last.unwrap()
        } else {
            let first = last.map(|x| na::DVector::<N>::from_column_slice(
                    x.as_slice()
            ));
            let second = other.control_points.first().map(
                |x| na::DVector::<N>::from_column_slice(x.as_slice()));
            return Err(MismatchedPoints {
                first,
                second,
                operation:
                    "When concatenating curves, the end point of the first and\
                    start point of the second must be the same.",
            });
        };
        self.control_points.extend((0..O::dim()).into_iter().map(|_| fill_pt.clone()));
        self.control_points.extend((&other.control_points).into_iter().map(|x| x.clone()));
        self.knot_vector.extend((&other.knot_vector).into_iter().map(|x| *x+param_max));
        Ok(())
    }
    /// Fixes parameter from [0, param_max] to [0, 1]
    fn normalize_parameter(&mut self, param_max: N) {
        self.knot_vector.iter_mut().map(|x| *x /= param_max).count();
    }
    /// Fits a spline to the provided points
    ///
    /// This will tend to give smooth-cornered approximations.
    pub fn global_interp(pts: Vec<na::OVector<N, D>>) -> Option<Self> {
        println!("pts len {}", pts.len());
        let n = pts.len()-1;
        // book r = D in this code
        let mut knot_bar = vec![];
        let mut net_chord = N::zero();
        for pt in pts.windows(2) {
            let chord = (pt[0].clone() - pt[1].clone()).norm();
            knot_bar.push(net_chord);
            net_chord += chord;
        }
        knot_bar.push(net_chord);
        log::trace!("chords accumulated");
        // u-bars are knot_bar/net_chord
        let mut knot_vector = vec![];
        // degree zeroes up front:
        for _i in 0..O::dim() {
            knot_vector.push(N::zero());
        }
        log::trace!("knots accumulated");
        // average u-bars into sensible vector -- skip initial 0
        for j in 1..=(n-(O::dim()-1)) {
            let mut c = N::zero();
            for i in j..j+(O::dim()-1) {
                c += knot_bar[i]/net_chord;
            }
            c /= N::from_usize(O::dim()-1).unwrap();
            knot_vector.push(c);
        }
        log::trace!("ubars accumulated");
        // degree ones on the tail:
        for _i in 0..O::dim() {
            knot_vector.push(N::one());
        }
        let kvl = knot_vector.len();
        println!("Knot vector: {:?}", knot_vector);
        let mut spline = BSpline {
            knot_vector,
            control_points: vec![na::zero(); kvl-(O::dim()-1)],
            order_phantom: PhantomData,
        };
        log::trace!("allocating matrix");
        // inefficient square matrix FTW
        let mut a: na::Matrix<N, na::Dynamic, na::Dynamic, _> = na::DMatrix::zeros(n+1, n+1);
        for i in 0..n {
            println!("row started");
            let uk = knot_bar[i]/net_chord;
            println!("finding span for {}", uk);
            let span = spline.find_span(uk);
            println!("finding basis for {}", uk);
            let basis_functions = spline.basis_functions(uk, span);
            println!("basis {:?}", basis_functions);
            for j in 0..(O::dim()) {
                println!("coords {}, {}", i, span-(O::dim()-1)+j);
                a[(i, span-(O::dim()-1)+j)] = basis_functions[j];
            }
            println!("row done");
        }
        a[(n, n)] = N::one();
        println!("Ready to start LU decomp of {}", a);
        log::trace!("Ready to start LU decomp");
        let a = a.lu();
        // solve each of the dimensions
        let mut col = vec![na::DVector::zeros(n+1); D::dim()];
        for j in 0..D::dim() {
            for i in 0..=n {
                col[j][i] = pts[i][j];
            }
            if !a.solve_mut(&mut col[j]) {
                return None;
            }
        }
        let mut control_points = vec![];
        for i in 0..=n {
            let mut p = na::OVector::<N, D>::zeros();
            for j in 0..D::dim() {
                p[j] = col[j][i];
            }
            control_points.push(p);
        }
        spline.control_points = control_points;
        Some(spline)
    }

}

/// Demotes a planar curve one dimension, if possible.
///
/// This presently assumes that the curve is zero in the last coordinate. Future versions may
/// relax this constraint. If this is not met, you will receive None.
// + DimDiff<D, U1> + DimNameDiff<D, U1>
pub fn demote_dimension<N, D, O>(b: &BSpline<N, DimNameSum<D, U1>, O>) -> Option<BSpline<N, D, O>> 
where
    N: RealField,
    D: DimName + DimNameAdd<U1> + DimNameMul<U1> + DimAdd<U1>
    + DimNameAdd<U1, Output = <D as DimAdd<U1>>::Output>,
    <D as DimAdd<U1>>::Output: DimAdd<U1>,
    O: DimName,
    na::DefaultAllocator: na::allocator::Allocator<N, DimNameSum<D, U1>> +
    na::allocator::Allocator<N, D>,
    DimNameSum<D, U1>: DimNameMul<U1>,
{
    let control_points_maybe = b.control_points.iter().map(
        |v| na::OVector::from_homogeneous(v.clone())
    ).collect();
    match control_points_maybe {
        Some(control_points) => 
        {
            Some(BSpline {
                control_points,
                knot_vector: b.knot_vector.clone(),
                order_phantom: b.order_phantom,
            })
        },
        _ => None,
    }
}

impl<N, D, O, S> BezierCurve<N, D, O, S> where
N: RealField,
D: DimName,
O: DimName,
S: na::storage::Storage<N, D, O>
{
    /// Constructs a new Bezier Curve whose control points are in the columns of the matrix.
    pub fn new(from: na::Matrix<N, D, O, S>) -> BezierCurve<N, D, O, S> {
        assert!(O::dim() >= 2, "Curves must have at least order 2");
        BezierCurve {
            control_points: from,
        }
    }
    /// Returns the value of the curve at a point.
    pub fn eval_at(&self, u: N) -> na::OVector<N, D> 
        where na::DefaultAllocator: na::allocator::Allocator<N, D, na::U1> {
            let mut acc = na::OVector::<N, D>::zeros();
            for (i, point) in self.control_points.column_iter().enumerate() {
                let sc = bernstein(u, self.order() as u32, i as u32);
                acc += point.scale(sc);
            }
            acc
        }
    /// Order of the Bezier curve. 
    /// 
    /// This is set when constructed from the input matrix.
    pub fn order(&self) -> usize {
        O::dim() - 1
    }
}

fn fact(k: u32) -> u32 {
    fact_fract(k, 1)
}

fn fact_fract(k: u32, j: u32) -> u32 {
    if k <= j {
        1
    } else {
        k*fact_fract(k-1, j)
    }
}

fn bernstein<N: RealField> (u: N, n: u32, i: u32) -> N {
    assert!(n > 0, "there is no zeroth-degree bernstein polynomial");
    assert!(i <= n, "there are only 0...n n-th degree bernstein polynomials");
    assert!(u >= N::zero() && u <= N::one(),
    "the bernstein polynomials are def'd on [0, 1]");
    let frac = fact_fract(n, i);
    let mut res = N::from_u32(frac)
        .expect("You must be able to represent integers.");
    res /= N::from_u32(fact(n-i))
        .expect("Field types must be able to represent integers"); // i!
    res *= u.powi(i as i32);
    res *= (N::one() - u).powi((n - i) as i32);
    res
}

/// Projects a vector up a dimension with the given weight.
///
fn project_up<N, D>(v: na::OVector<N, D>, w: N)
    -> na::OVector<N, DimNameSum<D, U1>> 
    where 
    N: RealField,
    D: DimNameAdd<U1> + DimName + DimAdd<U1, Output = <D as DimNameAdd<U1>>::Output>,
    na::DefaultAllocator: na::allocator::Allocator<N, DimNameSum<D, U1>> + 
    na::allocator::Allocator<N, D>,
    DimNameSum<D, U1>: DimNameMul<U1>,
{
    let intermed = v.scale(w);
    intermed.push(w)
}

/// Projects a vector down a dimension, from homogenous coordinates.
///
fn project_down<N, D>(v: &na::OVector<N, DimNameSum<D, U1>>)
    -> na::OVector<N, D> 
    where 
    N: RealField,
    D: DimName + DimNameAdd<U1>,
    na::DefaultAllocator: na::allocator::Allocator<N, DimNameSum<D, U1>> +
    na::allocator::Allocator<N, D>,
    DimNameSum<D, U1>: DimNameMul<U1>,
{
    let w = v[v.len()-1];
    if w == N::zero() {
        na::OVector::<N, D>::zeros()
    } else {
        let nrows = D::from_usize(v.len()-1);
        let intermed = v.unscale(w);
        intermed.generic_slice((0, 0), (nrows, U1::name())).into_owned()
    }
}

pub fn get_one_arc<N>(tangent0: na::Vector3<N>,tangent2: na::Vector3<N> ,point0: na::Vector3<N> ,point2: na::Vector3<N> ,point_on_arc : na::Vector3<N>)
    -> BSpline<N,na::U3,na::U3>
    where
    N: RealField
    {
    
    let (point,wheight_one) = make_one_arc(tangent0,tangent2,point0,point2,point_on_arc);
    let knot_vector= vec![N::zero(),N::zero(),N::zero(),N::one(),N::one(),N::one()];
    let control_points: Vec<na::Vector3<N>> = vec![point0,point,point2];
    BSpline::new(control_points, knot_vector)
    }
fn make_one_arc<N>(tangent0: na::Vector3<N>,tangent2: na::Vector3<N> ,point0: na::Vector3<N> ,point2: na::Vector3<N> ,point_on_arc : na::Vector3<N>)
       -> (na::Vector3<N>,N)
       where
       N:RealField
     {
     
     let mut temp_point_v02 = point2.sub(&point0);
     let i = intersect3d_lines(tangent0,tangent2,point0,point2); 
     if let Some((point,_,_)) = i {
     let mut temp_point_v1p = point_on_arc.sub(&point);// need to get point 1 from the option
     let temp_var = intersect3d_lines(temp_point_v1p,temp_point_v02,point,point0);
     let alpha = temp_var.unwrap();
     let  a = N::sqrt(alpha.2/(N::one()-alpha.2));
     let  u = a/(N::one()+a);
     let first = (N::one() -u)*(N::one()-u)*point_on_arc.sub(&point0).dot(&alpha.0.sub(&point_on_arc)); 
     let second = u*u*point_on_arc.sub(&point2).dot(&alpha.0.sub(&point_on_arc));
     let num = first + second;
     let den = (N::one()+N::one())*u*(N::one()-u)*alpha.0.sub(&point_on_arc).dot(&alpha.0.sub(&point_on_arc));
     let weight_one = num/den;
     (alpha.0,weight_one)
     }
     else{
     let weight_one =N::zero();
     let temp_var = intersect3d_lines(tangent0,temp_point_v02,point_on_arc,point0);
     let alpha = temp_var.unwrap();
     let a = N::sqrt(alpha.2/(N::one() -alpha.2));
     let u = a/(N::one() +a);
     let mut b = (N::one()+N::one())*u*(N::one() -u);
     b = -alpha.1*(N::one() -b)/b;
     let point1 =tangent0.scale(b);
     (point1,weight_one)
     }
    }
fn intersect3d_lines<N>(tangent0: na::Vector3<N>,tangent2: na::Vector3<N>,
                        point0: na::Vector3<N> ,point2: na::Vector3<N> )
    -> Option<(na::Vector3<N>,N,N)>
    where
    N: RealField,
{   
    //Uning matrix to solve for aplha0 and alpha2
    //In point2X - point0X = alpha0*tangent0x - alpha2*tangent2x
    //   point2y - point0y = alpha0*tangent0y - aplha2*tangent2y
    let m = na::Matrix2::from_columns(&[tangent0.xy(),tangent2.xy().scale(-N::one())]);
    let alpha = if let Some(inv) = m.try_inverse() {
        let point_intermediate = point2.xy() - point0.xy();
        inv*point_intermediate  
    }
    else{
        return None;
    };
    if tangent0.z.scale(alpha[0]) + point0.z == tangent2.z.scale(alpha[1]) + point2.z {
        Some((tangent0.scale(alpha[0]) + point0,alpha[0],alpha[1]))
    }
    else{
        None
    }
}

impl<N, D, O, S> ParametricCurve<N, D> for BezierCurve<N, D, O, S> where
N: RealField,
D: DimName,
O: DimName,
S: na::storage::StorageMut<N, D, O> + Clone,
na::DefaultAllocator: na::allocator::Allocator<N, D>,
{
    fn eval_at(&self, u: N) -> na::OVector<N, D> {
        self.eval_at(u)
    }
    /// Note: this allocates a new Vec to give to you every time!
    fn get_control_points(&self) -> Cow<[na::OVector<N, D>]> {
        let arr = self.control_points
            .column_iter().map(|x| x.into_owned()).collect::<Vec<na::OVector<N, D>>>();
        arr.into()
    }
    fn get_knots(&self) -> Cow<[N]> {
        let arr = vec![N::zero(), N::one()];
        arr.into()
    }
    fn get_knots_with_mult(&self) -> Cow<[(N, usize)]> {
        let arr = vec![(N::zero(), O::dim()), (N::one(), O::dim())];
        arr.into()
    }
    fn shift_control_point(&mut self, which: usize, by: &na::OVector<N, D>) {
        self.control_points.column_mut(which).add_assign(by);
    }
}

impl<N, D, O> ParametricCurve<N, D> for BSpline<N, D, O> where
N: RealField,
D: DimName + DimNameMul<U1>,
O: DimName,
na::DefaultAllocator: na::allocator::Allocator<N, D> + na::allocator::Allocator<N, O, U1> 
{
    fn eval_at(&self, u: N) -> na::OVector<N, D> {
        self.eval_at(u)
    }
    fn get_control_points(&self) -> Cow<[na::OVector<N, D>]> {
        (&self.control_points).into()
    }
    fn shift_control_point(&mut self, which: usize, by: &na::OVector<N, D>) {
        self.control_points[which] += by;
    }
    /// This allocates a new Vec every time!
    fn get_knots(&self) -> Cow<[N]> {
        let mut arr = vec![N::zero()];
        for ui in &self.knot_vector {
            if *arr.last().expect("unreachable") != *ui {
                arr.push(*ui);
            }
        }
        arr.into()
    }
    fn get_knots_with_mult(&self) -> Cow<[(N, usize)]> {
        let mut arr = vec![];
        let mut mult = 0;
        let mut trailing = N::zero();
        for ui in &self.knot_vector {
            if *ui == trailing {
                mult += 1;
            } else {
                arr.push((trailing, mult));
                mult = 1;
                trailing = *ui;
            }
        }
        arr.push((trailing, mult));
        arr.into()
    }
}

impl<N, D, O> ParametricCurve<N, D> for NURBS<N, D, O> where
N: RealField,
D: DimName + DimNameMul<U1> + Dim + DimAdd<U1> + DimNameAdd<U1>,
O: DimName,
na::DefaultAllocator: na::allocator::Allocator<N, D> 
+ na::allocator::Allocator<N, O, U1>
+ na::allocator::Allocator<N, DimSum<D, U1>>
+ na::allocator::Allocator<N, DimNameSum<D, U1>>,
DimNameSum<D, U1>: DimNameMul<U1>,
D: DimAdd<U1, Output = <D as DimNameAdd<U1>>::Output>,
{
    fn eval_at(&self, u: N) -> na::OVector<N, D> {
        project_down(&self.bspline.eval_at(u))
    }
    /// Allocates every time you call it!
    fn get_control_points(&self) -> Cow<[na::OVector<N, D>]> {
        let arr = self.bspline.control_points.iter()
            .map(|x| project_down(x)).collect::<Vec<na::OVector<N, D>>>();
        arr.into()
    }
    fn shift_control_point(&mut self, which: usize, by: &na::OVector<N, D>) {
        let which = &mut self.bspline.control_points[which];
        let intermed = by.scale(which[D::dim()]).to_homogeneous();
        *which += intermed;
    }
    /// This allocates a new Vec every time!
    fn get_knots(&self) -> Cow<[N]> {
        self.bspline.get_knots()
    }
    fn get_knots_with_mult(&self) -> Cow<[(N, usize)]> {
        self.bspline.get_knots_with_mult()
    }
}

impl<N, D, O> Concatenable<N, D, BSpline<N, D, O>> for BSpline<N, D, O> where
N: RealField,
D: DimName + DimNameMul<U1>,
O: DimName,
na::DefaultAllocator: na::allocator::Allocator<N, D> 
+ na::allocator::Allocator<N, O, U1>,
{
    type ResultingCurve = BSpline<N, D, O>;

    fn concat(&self, other: &BSpline<N, D, O>) -> 
        Result<Self::ResultingCurve, MismatchedPoints<N>> {
            let mut c = self.clone();
            c.concat_unnormalized(other, N::one())?;
            c.normalize_parameter(N::one()+N::one());
            Ok(c)
        }

}

impl<N, D, O> Concatenable<N, D, NURBS<N, D, O>> for NURBS<N, D, O> where
N: RealField,
D: DimName + DimNameAdd<U1> + Dim + DimAdd<U1> + DimNameMul<U1>,
O: DimName,
DimNameSum<D, U1>: DimNameMul<U1>,
na::DefaultAllocator: na::allocator::Allocator<N, D> 
+ na::allocator::Allocator<N, O, U1>
+ na::allocator::Allocator<N, DimSum<D, U1>>
+ na::allocator::Allocator<N, DimNameSum<D, U1>>,
DimNameSum<D, U1>: DimNameMul<U1>,
D: DimAdd<U1, Output = <D as DimNameAdd<U1>>::Output>,
{
    type ResultingCurve = NURBS<N, D, O>;

    fn concat(&self, other: &NURBS<N, D, O>) -> 
        Result<Self::ResultingCurve, MismatchedPoints<N>> {
            Ok(NURBS {
                bspline: self.bspline.concat(&other.bspline)?
            })
        }
}

impl<N, D, O, S> From<BezierCurve<N, D, O, S>> for BSpline<N, D, O>
where
N: RealField,
D: DimName + DimNameMul<U1>,
O: DimName,
na::DefaultAllocator: na::allocator::Allocator<N, D> + na::allocator::Allocator<N, O, U1>,
S: na::storage::Storage<N, D, O>
{
    fn from(bezier: BezierCurve<N, D, O, S>) -> Self {
        let control_points: Vec<na::OVector<N, D>> = bezier.control_points
            .column_iter().map(|x| x.into_owned()).collect();
        let knot_vector = [vec![N::zero(); O::dim()], vec![N::one(); O::dim()]].concat().to_vec();
        BSpline{control_points, knot_vector, order_phantom: PhantomData}
    }
}

impl<N, D, O> From<BSpline<N, D, O>> for NURBS<N, D, O>
where
N: RealField,
D: DimName + DimNameMul<U1> + DimNameAdd<U1> + DimAdd<U1, Output = <D as DimNameAdd<U1>>::Output>,
O: DimName,
na::DefaultAllocator: na::allocator::Allocator<N, D>
+ na::allocator::Allocator<N, DimNameSum<D, U1>>,
<D as DimAdd<U1>>::Output: DimNameMul<U1>
{
    fn from(spline: BSpline<N, D, O>) -> Self {
        let knot_vector = spline.knot_vector;
        let control_points = spline.control_points.into_iter().map(|x| project_up(x, N::one())).collect();
        NURBS {
            bspline: BSpline{control_points, knot_vector, order_phantom: PhantomData}
        }
    }
}

impl<N, D, O> DifferentiableCurve<N, D, O> for BSpline<N, D, O>
where
O: DimName + DimNameAdd<U1> + na::DimSub<U1>,
<O as na::DimSub<U1>>::Output: DimName,
N: RealField,
D: DimName + DimNameMul<U1> + DimNameAdd<U1>,
U1: na::DimMin<<O as na::DimSub<U1>>::Output>,
na::DefaultAllocator: na::allocator::Allocator<N, D>
+ na::allocator::Allocator<N, D, O>
+ na::allocator::Allocator<N, O, O>
+ na::allocator::Allocator<N, U1, D>
+ na::allocator::Allocator<N, U1, O>
+ na::allocator::Allocator<N, <D as DimNameAdd<U1>>::Output>
+ na::allocator::Allocator<N, O>,
{
    /// Gives the value of the curve and its derivatives at a specific parameter value.
    ///
    /// The columns of the resulting matrix are the derivative vectors. The zeroeth column has the
    /// zeroth derivative (i.e. the curve's value).
    fn eval_with_derivatives_at<ND>(&self, u: N, num_derivatives: ND) -> na::OMatrix<N, D, ND> 
    where
        ND: Dim + na::DimMin<na::DimDiff<O, U1>>,
        na::DefaultAllocator:  na::allocator::Allocator<N, D, ND>
        + na::allocator::Allocator<N, ND, O>
        + na::allocator::Allocator<N, na::DimMinimum<ND, na::DimDiff<O, U1>>, O>,
    {
        self.eval_with_derivatives_at(u, num_derivatives)
    }
}

impl<N, D, O> DifferentiableCurve<N, D, O> for NURBS<N, D, O>
where
DimNameSum<D, U1>: DimNameMul<U1>,
O: DimName + DimNameAdd<U1> + na::DimSub<U1>,
<O as na::DimSub<U1>>::Output: DimName + na::DimMin<U1>,
N: RealField,
D: DimName + DimNameMul<U1> + DimAdd<U1> + DimNameAdd<U1> + Dim
+ DimAdd<U1, Output = <D as DimNameAdd<U1>>::Output> + DimNameAdd<U1>,
U1: na::DimMin<<O as na::DimSub<U1>>::Output>,
na::DefaultAllocator: na::allocator::Allocator<N, D>
+ na::allocator::Allocator<N, D, O>
+ na::allocator::Allocator<N, O, O>
+ na::allocator::Allocator<N, U1, D>
+ na::allocator::Allocator<N, U1, O>
+ na::allocator::Allocator<N, U1, <D as DimNameAdd<U1>>::Output>
+ na::allocator::Allocator<N, <D as DimAdd<U1>>::Output>
+ na::allocator::Allocator<N, <D as DimNameAdd<U1>>::Output>
+ na::allocator::Allocator<N, na::DimMinimum<na::DimDiff<O, U1>, U1>, O>
+ na::allocator::Allocator<N, O>,
{
    /// Gives the value of the curve and its derivatives at a specific parameter value.
    ///
    /// Do NOT ask for more than 34 derivatives or there will be an overflow, which will either
    /// panic or silently fail based on your compiler settings.
    ///
    /// The columns of the resulting matrix are the derivative vectors. The zeroeth column has the
    /// zeroth derivative (i.e. the curve's value).
    fn eval_with_derivatives_at<ND>(&self, u: N, num_derivatives: ND) -> na::OMatrix<N, D, ND> 
    where
        ND: Dim + na::DimMin<na::DimDiff<O, U1>>,
        na::DefaultAllocator: na::allocator::Allocator<N, D, ND>
        + na::allocator::Allocator<N, na::DimMinimum<ND, na::DimDiff<O, U1>>, O>
        + na::allocator::Allocator<N, ND, O>
        + na::allocator::Allocator<N, <D as DimNameAdd<U1>>::Output, ND>
    {
        let mut out = na::OMatrix::zeros_generic(D::name(), num_derivatives);
        let hom_derivatives: na::OMatrix<N, DimSum<D, U1>, ND> = self.bspline.eval_with_derivatives_at(u, num_derivatives);
        // The Book wants two arrays here, one of the top coordinates as a vector, and one of the
        // weights stripped off into their own array. However, that's not really how we've laid out
        // our data, so we're going to note that since each column is a derivative vector, the
        // w-derivatives are the bottom row of hom_derivatives, and so the zeroeth w-derivative is
        // the bottom-left value (the zeroeth row being the actual value of the function)
        let w0 = hom_derivatives[(D::dim()-1, 0)];
        for k in 0..=num_derivatives.value() {
            // Strip off last coordinate
            let mut v = hom_derivatives.column_part(k, D::dim()-1).into_owned();
            for i in 1..=k {
                let temp = N::from_u32(binomial(k as u32, i as u32))
                    .expect("Field type MUST support integer conversion");
                let temp = temp*hom_derivatives[(D::dim()-1, i)];
                v.sub_assign(out.column(k-i).scale(temp));
            }
            let temp_pt = v.scale(N::one()/w0);
            out.column_mut(k).copy_from(&temp_pt);
        }
        out
    }
}

/*
    control_points: Vec<na::OVector<N, D>>,
    knot_vector: Vec<N>,
    */

impl<N, D, O> NURBS<N, D, O> where
N: RealField,
D: DimName + DimNameAdd<U1> + Dim + DimAdd<U1>,
O: DimName,
na::DefaultAllocator: na::allocator::Allocator<N, DimNameSum<D, U1>>
+ na::allocator::Allocator<N, O>,
DimNameSum<D, U1>: DimNameMul<U1>,
{
    /// Constructs a new NURBS curve from the provided knot vector and points
    ///
    /// Note that the control points are one dimension up, in projective space.
    fn new(points: Vec<na::OVector<N, DimNameSum<D, U1>>>, knot_vector: Vec<N>) -> Self {
        // Maybe check for infinite points here? They'd break the point inversion algo pretty
        // thoroughly.
        //
        // Maybe check for negative weights here? I can't think of anything they'd break here but
        // they do violate the strong convex hull property, which we do use in render code...
        NURBS {
            bspline: BSpline::<N, DimNameSum<D, U1>, O>::new(points, knot_vector),
        }
    }
}
