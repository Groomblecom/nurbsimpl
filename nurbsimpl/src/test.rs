use crate::*;
#[test]
fn d2o1() {
    let pts = na::Matrix2::new(1.0, 2.0, 2.0, 3.0);
    let pts_vec: Vec<na::Vector2<f32>> = pts.column_iter().map(|x| {
        x.into_owned()
    }).collect();

    let curve = BezierCurve::new(pts.clone());
    assert_eq!(curve.eval_at(0.5), na::Vector2::new(1.5, 2.5), 
               "midpoint must be midpoint of line.");
    assert_eq!(curve.eval_at(0.0), pts_vec[0], 
               "control points must be start and end.");
    assert_eq!(curve.eval_at(1.0), pts_vec[1], 
               "control points must be start and end.");
}
#[test]
fn test_bernstein() {
    for n in 1..10 {
        println!("\nRun for n={}", n);
        assert_eq!(bernstein(0.0f64, n, 0), 1.0, "0th bernstein poly starts at 1");
        assert_eq!(bernstein(1.0f64, n, n), 1.0, "nth bernstein poly ends at 1");
        for u in 0..11u32 {
            let u = u as f64/10.0;
            println!("\nRun for n={}, u={}", n, u);
            let mut acc = 0.0f64;
            for i in 0..(n+1) {
                acc += bernstein(u, n, i);
            }
            println!("Acc: {}", acc);
            assert!(f64::abs(acc - 1.0) < f64::EPSILON*6.0, "bernstein polys must partition unity");
        }
    }
}
#[test]
fn test_bspline_span() {
    let k = vec![0.0f32, 0.0, 0.0, 1.0, 1.0, 1.0];
    let pts = na::Matrix1x3::new(1.0, 2.0, 3.0);
    let pts_vec = pts.column_iter().map(|v| v.into_owned()).collect();
    let curve = BSpline::<f32, U1, na::U3> ::new(pts_vec, k);
    println!("span lower index at 0.5: {}", curve.find_span(0.5));
    assert_eq!(curve.find_span(0.5), 2);
}
#[test]
fn test_bspline_basis() {
    let k = vec![0.0f32, 0.0, 0.0, 1.0, 2.0, 3.0, 4.0, 4.0, 5.0, 5.0, 5.0];
    let onevec  = na::Vector1::new(1.0f32);
    let pts = vec![onevec; 8];
    let curve = BSpline::<_, _, na::U3>::new(pts, k);
    let u = 2.5;
    let span = curve.find_span(u);
    let basis_functions = curve.basis_functions(u, span);
    println!("basis functions at {}: {}", u, basis_functions);
    assert_eq!(basis_functions, na::Vector3::new(1.0/8.0, 6.0/8.0, 1.0/8.0));
}
#[test]
fn test_knot_refignment() {
    use na::Vector2 as v2;
    let k = vec![0.0f32,0.0,0.0,0.0,3.0/10.0,7.0/10.0,1.0,1.0,1.0,1.0];
    let pts = vec![v2::new(0.0,0.0),v2::new(1.0,3.0),v2::new(2.0,1.0),
    v2::new(3.0,2.0),v2::new(4.0,4.0),v2::new(5.0,0.0)];
    let curve = BSpline::<_,_,na::U4>::new(pts,k);
    let x = vec![3.0f32/20.0,11.0/20.0,17.0/20.0];
    let newcurve = curve.knot_refinement(&x);
    let nlen = newcurve.knot_vector.len();
    assert_eq!(newcurve.knot_vector.len(),13);
    assert_eq!(newcurve.knot_vector[nlen-1],1.0);
    assert_eq!(newcurve.knot_vector[nlen-2],1.0);
    assert_eq!(newcurve.knot_vector[nlen-3],1.0);
    assert_eq!(newcurve.knot_vector[nlen-4],1.0);
} 

#[test]
fn test_knot_regignment2() {
    use na::Vector2 as v2;
    let k = vec![0.0f32,0.0,1.0/2.0,1.0,1.0];
    let pts = vec![v2::new(0.0,0.0),v2::new(2.0,2.0),v2::new(4.0,0.0)];
    let curve = BSpline::<_,_,na::U2>::new(pts,k);
    let x = vec![3.0f32/20.0,11.0/20.0,17.0/20.0];
    let newcurve = curve.knot_refinement(&x);
    assert_eq!(newcurve.knot_vector.len(),8);

}
#[test]
fn test_knot_regignment3() {
    use na::Vector2 as v2;
    let k = vec![0.0f32,0.0,0.0,0.0,1.0,1.0,1.0,1.0];
    let pts = vec![v2::new(0.0,0.0),v2::new(1.0,2.0),v2::new(4.0,3.0),v2::new(5.0,0.0)];
    let curve = BSpline::<_,_,na::U4>::new(pts,k);
    let x = vec![3.0f32/20.0,11.0/20.0,17.0/20.0];
    let newcurve = curve.knot_refinement(&x);
    assert_eq!(newcurve.knot_vector.len(),11);

}
#[test]
fn test_bspline_eval() {
    let k = vec![0.0f32, 0.0, 0.0, 1.0, 2.0, 3.0, 4.0, 4.0, 5.0, 5.0, 5.0];
    let onevec  = na::Vector1::new(1.0f32);
    let pts = vec![onevec; 8];
    let curve = BSpline::<_, _, na::U3>::new(pts, k);
    let u = 2.5;
    let span = curve.find_span(u);
    let basis_functions = curve.basis_functions(u, span);
    println!("basis functions at {}: {}", u, basis_functions);
    let res = curve.eval_at(u);
    println!("value of curve: {}", res);
}
#[test]
fn test_bspline_derivative_eval() {
    let k = vec![0.0f32, 0.0, 0.0, 1.0, 1.0, 1.0];
    let pts = vec![na::Vector2::<f32>::new(-1.0, 1.0), na::Vector2::<f32>::new(0.0, 0.0), na::Vector2::<f32>::new(1.0, 1.0)];
    let curve = BSpline::<_, _, na::U3>::new(pts, k);
    let u = 0.5;
    let res = DifferentiableCurve::<f32, na::U2, na::U3>::eval_with_derivatives_at(&curve, u, na::U3::name());
    println!("value of curve: {}", res);
    assert_eq!(na::Vector2::<f32>::new(2.0, 0.0), res.column(1));
}

#[test]
fn test_intersect3d_lines() {
    let p0 = na::Vector3::new(1.0f32,1.0,1.0);
    let t0 = na::Vector3::new(2.0f32,3.0,1.0);
    let p2 = na::Vector3::new(1.0f32,1.0,1.0);
    let t2 = na::Vector3::new(1.0f32,1.0,1.0);
    let result= intersect3d_lines(t0,t2,p0,p2);
    let alpha  = result.unwrap();
    let alpha0 = alpha.1;
    let alpha2 = alpha.2;
    assert_eq!(alpha0,alpha2);

}

#[test]
fn test_bspline_concat() {
    let a = na::Vector2::<f32>::new(0.0, 0.0);
    let b = na::Vector2::<f32>::new(1.0, 1.0);
    let c = na::Vector2::<f32>::new(2.0, 0.0);
    let d = na::Vector2::<f32>::new(3.0,-1.0);
    let e = na::Vector2::<f32>::new(4.0, 0.0);
    let first = BSpline::<_, _, na::U3>::new(vec![a, b, c], 
                                             vec![0.0, 0.0, 0.0, 1.0, 1.0, 1.0]);
    let second = BSpline::<_, _, na::U3>::new(vec![c, d, e], 
                                             vec![0.0, 0.0, 0.0, 1.0, 1.0, 1.0]);
    let concat = first.concat(&second).unwrap();
    println!("Concatenated curve: {:?}", concat);
    for u in 0..10u32 {
        let u = u as f32/20.0;
        assert_eq!(concat.eval_at(u), first.eval_at(u*2.0), "concat must have same curve afterward");
    }
    for u in 10..20u32 {
        let u = u as f32/20.0;
        assert_eq!(concat.eval_at(u), second.eval_at(u*2.0-1.0), "concat must have same curve afterward");
    }
}

 #[test]
     fn test_knot_insertion(){
     	//pub fn insert_knot(&self,u: N,knot_index: usize,multiplicity: usize, subdivisions: usize){
        
    	use na::Vector2 as v2;
    	let k = vec![0.0f32, 0.0, 0.0, 0.0, 1.0/5.0, 2.0/5.0, 3.0/5.0, 4.0/5.0, 1.0, 1.0, 1.0, 1.0];
    	let k_len = k.len()+1;
    	let pts = vec![v2::new(0.0,0.0),v2::new(1.0,3.0),v2::new(2.0,1.0),
        v2::new(3.0,2.0),v2::new(4.0,4.0),v2::new(5.0,0.0),v2::new(5.0,0.0),v2::new(5.0,0.0)];
        let curve = BSpline::<_,_,na::U4>::new(pts,k);
        let u = 3.5/5.0;
        let knot_index = 6;
        let multiplicity = 0;
        let subdivisions = 1;
        let new_curve = curve.insert_knot(u, knot_index, multiplicity, subdivisions);
        println!("Knot vectors: {:?}",new_curve.knot_vector);
   }
    
 #[test]
    fn test_make_one_arc(){
    	use na::Vector3 as v3;
    	let point0 = v3::new(0.0,0.0,0.0);
    	let tangent0 = v3::new(0.0,-1.0,0.0);
    	let point2 = v3::new(4.0,0.0,0.0);
    	let tangent2 = v3::new(4.0,-1.0,0.0);
	let point_on_arc = v3::new(2.0,2.0,0.0);
	let new_curve = get_one_arc(tangent0,tangent2,point0,point2,point_on_arc);
    	assert_eq!(new_curve.control_points.len(),3)
    }   	
#[test]
fn test_bspline_point_finding() {
    let k = vec![0.0f32, 0.0, 0.0, 1.0, 1.0, 1.0];
    let pts = vec![na::Vector2::<f32>::new(-1.0, 1.0), na::Vector2::<f32>::new(0.0, 0.0), na::Vector2::<f32>::new(1.0, 1.0)];
    let curve = BSpline::<_, _, na::U3>::new(pts, k);
    let res = curve.point_inversion(&na::Vector2::<f32>::new(0.0, 0.0), true);
    println!("value of curve: {:?}", res);
    let res = curve.point_inversion(&na::Vector2::<f32>::new(-1.0, 1.0), true);
    println!("value of curve: {:?}", res);
    assert_eq!(res.1, 0.0, "Endpoints should be indeed mapped to endpoints");
    assert_eq!(res.0, 0.0, "Endpoints should be indeed mapped to endpoints");
    let res = curve.point_inversion(&na::Vector2::<f32>::new(1.0, 1.0), true);
    println!("value of curve: {:?}", res);
    assert_eq!(res.1, 0.0, "Endpoints should be indeed mapped to endpoints");
    assert_eq!(res.0, 1.0, "Endpoints should be indeed mapped to endpoints");
}
#[test]
fn test_knot_removal(){
  
    let a = na::Vector2::<f32>::new(0.0, 0.0);
    let b = na::Vector2::<f32>::new(1.0, 1.0);
    let c = na::Vector2::<f32>::new(2.0, 0.0);
    let d = na::Vector2::<f32>::new(3.0,-1.0);
    let e = na::Vector2::<f32>::new(4.0, 0.0);
    let mut first = BSpline::<_, _, na::U4>::new(vec![a,b,c,c,d,e], 
                                             vec![0.0,0.0, 0.0, 0.0, 0.5, 0.5, 1.0,1.0,1.0,1.0]);
    
    let tol = 0.1/(1.0+na::Vector2::new(3.0,-1.0).norm()as f32);
    first.knot_removal(5,1,0.5,2,tol);
    
    /*for u in 0..10{
    	let u = u as f32/ 10.0;
    	assert!(oldcurve.eval_at(u).metric_distance(&curve.eval_at(u)) < 0.1,
    	"Difference between old and new curves must be bounded by a tolerence")
    
    }*/
    println!("Knot vectors: {:?}",first.get_knot_vector());
    assert_eq!(first.get_knot_vector().len(),9);
}

