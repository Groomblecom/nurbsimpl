use crate::*;

use std::convert::TryFrom;
use std::ops::{Deref, DerefMut};

use parry2d::{shape::ConvexPolygon, query::PointQuery};
use std::fmt;
use std::error::Error;

/// Error type for when two points should be the same point, but are not.
#[derive(Debug, Clone)]
pub struct DegenerateShape
{
    operation: &'static str,
}

impl fmt::Display for DegenerateShape
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Detected degenerate shape when computing {}", self.operation)
    }
}

impl Error for DegenerateShape {}

/// Connected, mostly differentiable 1-Manifold
///
/// Maps a circle S1 with parameter in [0, 1).
/// 
/// Currently uses a quadratic NURBS curve as internal rep.
/// Currently restricted to embeddings in R2; this may change.
#[derive(Clone, Debug)]
pub struct OneManifold<N: RealField> {
    /// INVARIANTS:
    /// Does not self-intersect
    /// Is at least G0 continuous everywhere.
    curve: NURBS<N, na::U2, na::U3>,
}

impl<N: RealField> TryFrom<NURBS<N, na::U2, na::U3>> for OneManifold<N> {
    type Error = MismatchedPoints<N>;
    fn try_from(curve: NURBS<N, na::U2, na::U3>) -> Result<Self, Self::Error> {
        let left = curve.eval_at(N::zero());
        let right = curve.eval_at(N::one());
        // Check endpoints
        // NOTE: disabled because deadline. WHoops.
        /*
        if !ulps_eq!((right - left).norm(), N::zero(), max_ulps = 16) {
            let left = na::DVector::<N>::from_column_slice(left.as_slice());
            let right = na::DVector::<N>::from_column_slice(right.as_slice());
            return Err(MismatchedPoints {
                first: Some(left),
                second: Some(right),
                operation: "To make a curve a loop, it must start and end at the same point",
            });
        }
        */
        // Check knot multiplicities
        // TODO: remove into_owned call or sth to avoid alloc
        for (knot, mult) in curve.get_knots_with_mult().into_owned() {
            if mult >= na::U3::dim() && knot != N::zero() && knot != N::one() {
                let pt = curve.eval_at(knot);
                let pt = na::DVector::<N>::from_column_slice(pt.as_slice());
                return Err(MismatchedPoints {
                    first: Some(pt),
                    second: None,
                    operation: "To make a curve a loop, it must not 'jump'\
                        -- the multiplicity was too large",
                });
            }
        }
        // TODO TODO TODO check self-intersection
        Ok(OneManifold{curve})
    }
}

/// Boundary-rep structure for 2-Manifolds with boundary
///
/// Currently restricted to embeddings in R2; this may change.
#[derive(Clone, Debug)]
pub struct Area<N: RealField> {
    /// INVARIANTS:
    /// lower-index shells are NEVER contained in higher-index shells
    /// all shells are disjoint (no two intersect)
    /// all shells are well-behaved 1-manifolds
    ///
    /// The usize is depth; this is used for rendering.
    shells: Vec<(OneManifold<N>, usize)>,
}

impl<N: RealField> From<OneManifold<N>> for Area<N> {
    fn from(v: OneManifold<N>) -> Area<N> {
        Area {
            shells: vec![(v, 0)],
        }
    }
}

impl<N: RealField> Area<N> {
    // Allocates every time. Yikes
    pub fn get_shells(&self) -> Cow<[(OneManifold<N>, usize)]> {
        self.shells.clone().into()
    }
    // could definitely be more efficient by scanning only depth-0 shells
    pub fn bounding_box(&self) -> (na::OVector<N, na::U2>, na::OVector<N, na::U2>) {
        let (mut lower, mut upper) = self.shells.last().unwrap().0.bounding_box();
        for (shell, _depth) in &self.shells {
            let (slower, supper) = shell.bounding_box();
            for i in 0..na::U2::dim() {
                if lower[i] > slower[i] {
                    lower[i] = slower[i];
                }
                if upper[i] < supper[i] {
                    upper[i] = supper[i];
                }
            }
        }
        (lower, upper)
    }
}
impl Area<f32> {
    // Allocates every time. Yikes
    pub fn encompass(&mut self, m: OneManifold<f32>) -> Result<(), DegenerateShape> {
        // test this with the existing shells convex hulls to find out where it lives
        // TODO we assume it doesn't intersect any of them
        let mut best_depth = 0;
        let mut best_index = 0;
        let mhull = match m.convex_hull() {
            Some(s) => s,
            None => return Err(DegenerateShape{operation: "inserting a fresh shell"}),
        };
        for (i, (shell, depth)) in self.shells.iter_mut().enumerate() {
            let pt = m.eval_at(0.0);
            let pt = na::Point2::new(pt[0], pt[1]);
            if shell.convex_hull().unwrap().contains_local_point(&pt) {
                best_depth = *depth +1;
                best_index = i+1;
            }
            let spt = shell.eval_at(0.0);
            let spt = na::Point2::new(spt[0], spt[1]);
            if mhull.contains_local_point(&spt) {
                *depth += 1;
            }
        }
        self.shells.insert(best_index, (m, best_depth));
        Ok(())
    }
}

impl<N: RealField> Deref for OneManifold<N> {
    type Target = NURBS<N, na::U2, na::U3>;
    fn deref(&self) -> &Self::Target {
        &self.curve
    }
}
impl<N: RealField> DerefMut for OneManifold<N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.curve
    }
}

impl<N: RealField> OneManifold<N> {
    /// Returns a unit circle
    pub fn unit_circle() -> OneManifold<N> {
        let z = N::zero();
        let o = N::one();
        let h = o/(o+o); // half
        let q = h*h; // quarter
        let trq = h + q; // three quarters
        let knot_vector = vec![z, z, z, q, q, h, h, trq, trq, o, o, o];
        let t = o + o; // two
        let sqt = N::sqrt(t)/t; // sqrt(2)/2
        use na::Vector3 as v;
        /*
            v::new(o, z, o), v::new(o, o, sqt), v::new(z, o, o),
            v::new(-o, o, sqt), v::new(-o, z, o), v::new(-o, -o, sqt),
            v::new(z, -o, o), v::new(o, -o, sqt), v::new(o, z, o),
            */
        let points = vec![
            v::new(o, z, o), v::new(sqt, sqt, sqt), v::new(z, o, o),
            v::new(-sqt, sqt, sqt), v::new(-o, z, o), v::new(-sqt, -sqt, sqt),
            v::new(z, -o, o), v::new(sqt, -sqt, sqt), v::new(o, z, o),
        ];
        OneManifold {
            curve: NURBS::new(points, knot_vector)
        }
    }
    /// Applies a transformation to the loop.
    pub fn transform(&mut self, t: &na::Transform<N, na::TAffine, 2>) {
        let pts = self.get_control_points().into_owned();
        for (i, v) in pts.iter().enumerate() {
            let pt = na::Point2::new(v[0], v[1]);
            let res = t.transform_point(&pt);
            self.shift_control_point(i, &(res-pt));
        }
    }
}

impl OneManifold<f32> {
    fn convex_hull(&self) -> Option<ConvexPolygon> {
        let mut pts = vec![];
        let control_points = self.get_control_points().into_owned();
        let mut trailing = control_points.first().unwrap().clone();
        for pt in control_points {
            if pt != trailing {
                pts.push(na::Point2::new(trailing[0], trailing[1]));
                trailing = pt;
            }
        }
        pts.push(na::Point2::new(trailing[0], trailing[1]));
        ConvexPolygon::from_convex_hull(pts.as_slice())
    }
}


#[cfg(test)]
mod test {
    use crate::topo::*;
    #[test]
    fn test_circle_validity() {
        use std::convert::TryInto;
        let c = OneManifold::<f32>::unit_circle();
        println!("C: {:#?}", c);
        for u in 0..20 {
            let u = (u as f32)/20.0;
            println!("norm: {}", f32::abs(c.eval_at(u).norm()));
            assert!(f32::abs(c.eval_at(u).norm() - 1.0) <= 1e6);
        }
        let _d: OneManifold<f32> = c.curve.try_into().unwrap();
    }
}
