init();

async function init() {
	// ns = NURBSImpl module
	const [ns] = await Promise.all([
		import("./demo-wasm/nurbsimpl_demo_wasm.js"),
	]);
	await ns.default();
	ns.start();
	console.log(ns);
	let canvas = document.getElementById("plot-canvas");
	// Fix the canvas's width and height
	canvas.width = canvas.clientWidth;
	canvas.height = canvas.clientHeight;
	ns.make_plot(parseInt(canvas.dataset.which));
	let plot_chooser = document.getElementById("plot-chooser");
	plot_chooser.onchange = async function(e) {
		ns.make_plot(parseInt(e.target.value));
	}
	canvas.onmousedown = function(e) {
		ns.select_control_point(e.offsetX, e.offsetY);
	}
	canvas.onmousemove = function(e) {
		ns.drag_control_point(e.offsetX, e.offsetY);
	}
	canvas.onmouseup = canvas.onmouseout = function(e) {
		ns.reset_control_point(e.offsetX, e.offsetY);
	}
}
