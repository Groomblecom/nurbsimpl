init();

async function init() {
	// ns = NURBSImpl module
	const [ns] = await Promise.all([
		import("./webui/nurbsimpl_webui.js"),
	]);
	await ns.default();
	ns.start();
	console.log(ns);
	let canvas = document.getElementById("ui-canvas");
	let text = document.getElementById("ui-code");
	let output = document.getElementById("ui-output");
	// Fix the canvas's width and height
	canvas.width = canvas.clientWidth;
	canvas.height = canvas.clientHeight;
	function update_plot() {
		let res = ns.run_program(canvas, text.value.trim());
		while(output.firstChild) {
			output.removeChild(output.firstChild);
		}
		let m = document.createTextNode("Out: "+res.message);
		output.appendChild(m);
		output.style.color = res.was_error?"red":"dimgray";
	}
	text.onchange = update_plot;
	update_plot();
}
