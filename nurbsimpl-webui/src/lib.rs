use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::HtmlCanvasElement;

use nurbsimpl_lang as nl;

use nalgebra as na;

use nurbsimpl::{topo::{OneManifold, Area}, ParametricCurve};

use log::{Level, trace};

#[wasm_bindgen]
pub fn start() {
    console_log::init_with_level(Level::Trace).unwrap();
    trace!("Hello from NURBSImpl!");
}


#[wasm_bindgen]
pub struct UiMessage {
    message: String,
    pub was_error: bool,
}

#[wasm_bindgen]
impl UiMessage {
    #[wasm_bindgen(getter)]
    pub fn message(&self) -> String {
        self.message.clone()
    }
}

#[wasm_bindgen]
pub fn run_program(canvas: HtmlCanvasElement, program: String) -> UiMessage {
    let res = nl::run("webinputprogram".to_string(), &program);
    match res {
        Ok(g) => {
            match g.get_area() {
                Some(a) => draw_area(canvas, a),
                None => {
                    match g.get_loop() {
                        Some(l) => draw_loop(canvas, l),
                        None => {},
                    }
                },
            }
            UiMessage {
                message: g.to_string(),
                was_error: false,
            }
        },
        Err(e) => {
            UiMessage {
                message: e.to_string(),
                was_error: true,
            }
        }
    }
}

fn draw_loop(canvas: HtmlCanvasElement, l: OneManifold<f32>) {
    let c_width = canvas.width() as f32;
    let c_height = canvas.height() as f32;
    let (lower, upper) = l.bounding_box();
    let o_width = upper.x - lower.x;
    let o_height = upper.y - lower.y;
    let ratio = f32::min(c_width/o_width, c_height/o_height);
    let scale = |v: na::Vector2<f32>| -> (f64, f64) {
        let p = ((v.x - lower.x)*ratio, (v.y - lower.y)*ratio);
        (p.0 as f64, p.1 as f64)
    };

    let context = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();

    context.clear_rect(0.0, 0.0, c_width as f64, c_height as f64);

    // TODO: make this bound error
    context.begin_path();
    let p = scale(l.eval_at(0.0));
    context.move_to(p.0, p.1);
    for u in 1..=300 {
        let u = (u as f32)/300.0;
        let p = scale(l.eval_at(u));
        context.line_to(p.0, p.1);
    }
    context.stroke();
}

fn draw_area(canvas: HtmlCanvasElement, a: Area<f32>) {
    let c_width = canvas.width() as f32;
    let c_height = canvas.height() as f32;
    let (lower, upper) = a.bounding_box();
    let o_width = upper.x - lower.x;
    let o_height = upper.y - lower.y;
    let ratio = f32::min(c_width/o_width, c_height/o_height);
    let scale = |v: na::Vector2<f32>| -> (f64, f64) {
        let p = ((v.x - lower.x)*ratio, (v.y - lower.y)*ratio);
        (p.0 as f64, p.1 as f64)
    };

    let context = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();

    context.clear_rect(0.0, 0.0, c_width as f64, c_height as f64);

    for (l, depth) in a.get_shells().into_owned() {
        // TODO: make this bound error
        context.begin_path();
        let p = scale(l.eval_at(0.0));
        context.move_to(p.0, p.1);
        for u in 1..=300 {
            let u = (u as f32)/300.0;
            let p = scale(l.eval_at(u));
            context.line_to(p.0, p.1);
        }
        if depth%2 == 0 {
            context.set_fill_style(&JsValue::from_str("#888"));
        } else {
            context.set_fill_style(&JsValue::from_str("#FFF"));
        }
        context.fill();
    }
}
