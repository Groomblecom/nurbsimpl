use nurbsimpl_demo_wasm::{make_plot_maybe, num_plots_available};

use futures::executor::block_on;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    for plot in 1..num_plots_available() {
        block_on(make_plot_maybe(plot))?;

    }
    Ok(())
}
