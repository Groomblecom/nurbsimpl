use clap::{AppSettings, Clap};

use env_logger::{Env, Builder};

use std::fs;

use nurbsimpl_lang::run;

#[derive(Clap)]
#[clap(version = "1.0", author = "Noah Cowie <noah@instrumented.space>")]
#[clap(setting = AppSettings::ColoredHelp)]
struct Opts {
    input_name: String,
}

fn main() {
    Builder::from_env(Env::default().default_filter_or("debug")).init();
    let opts = Opts::parse();

    // load input file
    let prog = fs::read_to_string(&opts.input_name)
        .expect("Cannot open supplied program file");
    let res = run(opts.input_name, &prog);
    match res {
        Ok(s) => println!("{}", s.to_string()),
        Err(e) => println!("{}", e.to_string()),
    }
}
