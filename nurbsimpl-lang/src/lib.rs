use log::{trace, info};
use std::error::Error;
use std::fmt;
use std::fmt::Debug;
use std::collections::HashMap;
use std::convert::TryInto;

use nom::error::VerboseError;

use nurbsimpl::{topo::{OneManifold, Area, DegenerateShape}, ParametricCurve, BSpline, NURBS, MismatchedPoints};
use nalgebra as na;
use simba::scalar::SubsetOf;

use ast::*;

/// Parses and describes syntax trees for NURBSimpl lang programs
mod ast;

/// Error type for both syntax and runtime errors
#[derive(Debug, Clone)]
pub enum NurbsimplError<'a>
{
    Parse(nom::Err<VerboseError<Span<'a>>>),
    UnableToFinish(Span<'a>),
    RecursiveDecl(String, VariableDecl<'a>),
    ReusedName(Span<'a>),
    NonexistentName(Environment<'a>, NameScope<'a>, String, Span<'a>),
    NotCallable(Span<'a>),
    TypeMismatch(Typ, Typ, Span<'a>),
    MultipleReturn(ReturnStatement<'a>, ReturnStatement<'a>),
    TypeMissing(Span<'a>),
    ArgMissing(Span<'a>),
    BuiltinArgsMissing(&'static str),
    DegenerateShape(DegenerateShape),
    MismatchedPoints(MismatchedPoints<f32>),
}

impl<'a> fmt::Display for NurbsimplError<'a>
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            Self::Parse(s) => write!(f, "Parse error: {:#?}", s),
            Self::RecursiveDecl(name, decl) => {
                write!(f, 
                       r#"{}, declared at {:?} \
                       recursively references itself "#,
                       name, decl) 
            },
            Self::UnableToFinish(badbit) => {
                write!(f, "Could not parse rest of file starting at {:#?}", badbit) 
            },
            Self::TypeMismatch(t, u, loc) => {
                write!(f, "Expected {:?} but found {:?} at {:?}", t, u, loc) 
            },
            Self::ReusedName(name) => {
                write!(f, "variable {:?} declared twice in same scope!", name) 
            },
            Self::NonexistentName(env, scope, name, loc) => {
                write!(f, "variable {:?} used at {:?} but never declared! \
                       State {:?} and currently resolving scope {:?}", name, loc, env, scope) 
            },
            Self::NotCallable(name) => {
                write!(f, "Variable {:?} exists but is not a function and cannot be called!", name) 
            },
            Self::MultipleReturn(r1, r2) => {
                write!(f, "A region can only have one return statement;\
                however, you have {:?} followed by {:?}", r2, r1) 
            },
            Self::TypeMissing(t) => {
                write!(f, "You use a type that does not exist: {:?}", t)
            },
            Self::ArgMissing(a) => {
                write!(f, "Missing argument declared at: {:?}", a)
            },
            Self::BuiltinArgsMissing(a) => {
                write!(f, "Builtin function was missing arguments or had mismatched type. {:?}", a)
            },
            Self::DegenerateShape(a) => {
                write!(f, "Geometric operation failed due to bad (maybe too small?) shape. {:?}", a)
            },
            Self::MismatchedPoints(a) => {
                write!(f, "{}", a)
            },
        }
    }
}

impl<'a> From<nom::Err<VerboseError<ast::Span<'a>>>> for NurbsimplError<'a> {
    fn from(o: nom::Err<VerboseError<ast::Span<'a>>>) -> NurbsimplError<'a> {
        Self::Parse(o)
    }
}

impl From<MismatchedPoints<f32>> for NurbsimplError<'static> {
    fn from(o: MismatchedPoints<f32>) -> NurbsimplError<'static> {
        Self::MismatchedPoints(o)
    }
}

impl From<DegenerateShape> for NurbsimplError<'static> {
    fn from(o: DegenerateShape) -> NurbsimplError<'static> {
        Self::DegenerateShape(o)
    }
}

impl<'a> Error for NurbsimplError<'a> {}

/// This holds the namespaces, type information, and intermediate results of a
/// program.
#[derive(Debug, Clone)]
pub struct Environment<'a> {
    /// Holds the current stack of scopes, with higher index taking precidence
    scope_stack: Vec<NameScope<'a>>,
}

/// Holds resolved variables and tracks in-progress ones to detect loops.
#[derive(Debug, Clone)]
pub struct NameScope<'a> {
    variables: HashMap<String, Object<'a>>,
    to_resolve: HashMap<String, ResolvingProgress<'a>>,
}

#[derive(Debug, Clone)]
enum ResolvingProgress<'a> {
    Unresolved(ast::VariableDecl<'a>),
    Resolving(ast::VariableDecl<'a>),
    // resolved variables get kicked from this list.
}

/// Represents the runtime values of expressions
#[derive(Debug, Clone)]
enum Object<'a> {
    Null,
    Numeric(f32),
    Func(Callable<'a>),
    Loop(OneManifold<f32>),
    Area(Area<f32>),
}

/// Represents the runtime values of expressions
/// 
/// Hides internal type to allow changes
#[derive(Debug, Clone)]
pub struct ObjectHandle<'a>(Object<'a>);

impl<'a> ObjectHandle<'a> {
    /// Returns a human-readable string representation of this object.
    pub fn to_string(&self) -> String {
        match &self.0 {
            Object::Null => "".to_string(),
            Object::Numeric(n) => format!("{}", n),
            Object::Func(f) => {
                let sig = f.get_signature();
                format!("function {:?} -> {:?}", sig.0, sig.1)
            }
            Object::Loop(l) => {
                let p = l.eval_at(0.0);
                format!("Loop from ({}, {})", p.x, p.y)
            },
            Object::Area(_a) => {
                format!("Area")
            },
        }
    }
    /// Copies the associated loop geometry, if any.
    pub fn get_loop(&self) -> Option<OneManifold<f32>> {
        match &self.0 {
            Object::Loop(l) => Some(l.clone()),
            _ => None,
        }
    }
    pub fn get_area(&self) -> Option<Area<f32>> {
        match &self.0 {
            Object::Area(l) => Some(l.clone()),
            _ => None,
        }
    }
}

impl Object<'_> {
    fn typ(&self) -> Typ{
        match self {
            Self::Null => Typ::Null,
            Self::Numeric(_) => Typ::Numeric,
            Self::Func(_) => Typ::Func,
            Self::Loop(_) => Typ::Loop,
            Self::Area(_) => Typ::Area,
        }
    }
}

/// Represents the static types of expressions and their values.
#[derive(Debug, Clone, enum_utils::FromStr, PartialEq)]
pub enum Typ {
    Null,
    Numeric,
    // TODO make this hold the signature? idk.
    Func,
    Loop,
    Area,
}

impl<'a> Environment<'a> {
    fn basic() -> Environment<'a> {
        Environment {
            scope_stack: vec![NameScope::basic()],
        }
    }
}

impl<'a> NameScope<'a> {
    fn basic() -> NameScope<'a> {
        let mut provided = HashMap::new();
        provided.insert("PI".to_string(), Object::Numeric(std::f32::consts::PI));
        provided.insert("echo".to_string(), Object::Func(
                Callable::Echo
                ));
        provided.insert("cos".to_string(), Object::Func(
                Callable::BuiltinSimple(f32::cos)
                ));
        provided.insert("sin".to_string(), Object::Func(
                Callable::BuiltinSimple(f32::sin)
                ));
        provided.insert("circle".to_string(), Object::Func(
                Callable::LoopPrimitive(OneManifold::unit_circle())
                ));
        provided.insert("fill".to_string(), Object::Func(
                Callable::AreaPromotion
                ));
        provided.insert("encompass".to_string(), Object::Func(
                Callable::AreaEncompass
                ));
        provided.insert("translateloop".to_string(), Object::Func(
                Callable::LoopTransform(|x, y| {
                    na::Translation::<f32, 2>::new(x, y).to_superset()
                })
                ));
        provided.insert("scaleloop".to_string(), Object::Func(
                Callable::LoopTransform(|x, y| {
                    let m = na::Matrix3::new(x, 0.0, 0.0,
                                             0.0, y, 0.0,
                                             0.0,0.0,1.0);
                    na::Transform::<_, na::TAffine, 2>::from_matrix_unchecked(m)
                })
                ));
        provided.insert("fitloop".to_string(), Object::Func(
                Callable::FitLoop));
        NameScope {
            variables: provided,
            to_resolve: HashMap::new(),
        }
    }
}

#[derive(Debug, Clone)]
enum Callable<'a> {
    BuiltinSimple(fn(f32)->f32),
    Echo,
    User(Region<'a>, HashMap<Span<'a>, Typ>, (Typ, Span<'a>)),
    LoopPrimitive(OneManifold<f32>),
    LoopTransform(fn(f32, f32) -> na::Transform<f32, na::TAffine, 2>),
    AreaPromotion,
    AreaEncompass,
    FitLoop,
}

/// this does wayyyyyy too many allocations, TODO fix
impl<'a> Callable<'a> {
    fn call(&self, args: HashMap<String, Object<'a>>, env: &mut Environment<'a>) -> Result<Object<'a>, NurbsimplError<'a>> {
        match self {
            Self::BuiltinSimple(f) => {
                assert_eq!(args.len(), 1, "primitive R->R functions take 1 argument");
                let n = args.get("N")
                    .expect("primitive R->R functions have argument N and no others");
                if let Object::Numeric(n) = n {
                    Ok(Object::Numeric((f)(*n)))
                } else {
                    unreachable!();
                }
            },
            Self::Echo => {
                assert_eq!(args.len(), 1, "echo just prints a single numeric N");
                let n = args.get("N")
                    .expect("echo just prints a single numeric N");
                if let Object::Numeric(n) = n {
                    info!("ECHO: {}", n);
                    Ok(Object::Null)
                } else {
                    unreachable!();
                }
            },
            Self::User(r, arg_spec, (rettyp, retspan)) => {
                // Ensure provided variables match in type and name
                for (var, typ) in arg_spec {
                    if let Some(s) = args.get(*var.fragment()) {
                        if *typ != s.typ() {
                            return Err(NurbsimplError::TypeMismatch(
                                    typ.clone(), s.typ(), var.clone()));
                        } 
                    } else {
                        return Err(NurbsimplError::ArgMissing(var.clone()));
                    }
                }
                let scope = NameScope {
                    variables: args,
                    to_resolve: HashMap::new(),
                };
                env.scope_stack.push(scope);
                let ret = interpret_region(r, env)?;
                env.scope_stack.pop();
                // ensure rettype correct
                if ret.typ() != *rettyp {
                    Err(NurbsimplError::TypeMismatch(ret.typ(), rettyp.clone(), retspan.clone()))
                } else {
                    Ok(ret)
                }
            },
            Self::LoopPrimitive(l) => {
                Ok(Object::Loop(l.clone()))
            },
            Self::LoopTransform(m) => {
                let (x, y, geo) = match (args.get("X"), args.get("Y"), args.get("GEO")) {
                    (Some(x), Some(y), Some(geo)) => (x, y, geo),
                    _ => {
                        return Err(NurbsimplError::BuiltinArgsMissing("Translate needs X, Y and GEO"));
                    }
                };
                if let (Object::Numeric(x), Object::Numeric(y), Object::Loop(geo)) = (x, y, geo) {
                    let mut geo = geo.clone();
                    geo.transform(&(m)(*x, *y));
                    Ok(Object::Loop(geo))
                } else {
                    Err(NurbsimplError::BuiltinArgsMissing("Translate needs X, Y numerics and GEO a loop"))
                }
            },
            Self::AreaPromotion => {
                let geo = match args.get("GEO") {
                    Some(geo) => geo,
                    _ => {
                        return Err(NurbsimplError::BuiltinArgsMissing("Area filling promotion GEO a loop"));
                    }
                };
                if let Object::Loop(geo) = geo {
                    Ok(Object::Area(geo.clone().into()))
                } else {
                    Err(NurbsimplError::BuiltinArgsMissing("Area filling promotion GEO a loop"))
                }
            },
            Self::AreaEncompass => {
                let (area, geo) = match (args.get("IN"), args.get("GEO")) {
                    (Some(area), Some(geo)) => (area, geo),
                    _ => {
                        return Err(NurbsimplError::BuiltinArgsMissing("arguments IN, GEO required: \
                                                                      Area IN encompasses loop GEO"));
                    }
                };
                if let (Object::Area(area), Object::Loop(geo)) = (area, geo) {
                    let mut area = area.clone();
                    area.encompass(geo.clone())?;
                    Ok(Object::Area(area))
                } else {
                    Err(NurbsimplError::BuiltinArgsMissing("Type error; \
                                                           remember, Area IN encompasses loop GEO"))
                }
            },
            Self::FitLoop => {
                let (x, y) = match (args.get("X"), args.get("Y")) {
                    (Some(x), Some(y)) => (x, y),
                    _ => {
                        return Err(NurbsimplError::BuiltinArgsMissing(
                                "fitloop takes two functions u: Numeric -> Numeric and u \
                                                                       is in [0, 1]."));
                    }
                };
                if let (Object::Func(x), Object::Func(y)) = (x, y) {
                    // NOTE: tune this or make it smarter
                    let mut pts = vec![];
                    let mut args = HashMap::new();
                    for u in 0..=100 {
                        let u = u as f32/100.0;
                        args.insert("u".to_string(), Object::Numeric(u));
                        // call them
                        let resx = x.call(args.clone(), env)?;
                        let resy = y.call(args.clone(), env)?;
                        let (ptx, pty) = match (resx, resy) {
                            (Object::Numeric(ptx), Object::Numeric(pty)) => (ptx, pty),
                            _ => {
                            return Err(NurbsimplError::BuiltinArgsMissing(
                                    "fitloop takes two functions u: Numeric -> Numeric and u \
                                                                           is in [0, 1]."));
                            }
                        };
                        pts.push(na::Vector2::new(ptx, pty));
                    }
                    trace!("accumulated points for global approx");
                    // ok, now actually fit the thing
                    let spline: BSpline<f32, na::U2, na::U3> = match BSpline::global_interp(pts) {
                        Some(s) => s,
                        None => {
                            return Err(NurbsimplError::BuiltinArgsMissing(
                                    "Your functions are correctly-shaped but provided degenerate \
                                    input to fitloop"));
                        }
                    };
                    trace!("globally interpolated");
                    let n: NURBS<f32, na::U2, na::U3> = spline.into();
                    let l = n.try_into()?;
                    Ok(Object::Loop(l))
                } else {
                        Err(NurbsimplError::BuiltinArgsMissing(
                                "fitloop takes two functions u: Numeric -> Numeric and u \
                                                                       is in [0, 1]."))
                }
            },
        }
    }
    fn get_signature(&self) -> (HashMap<String, Typ>, Typ) {
        let mut ret = HashMap::new();
        match self {
            Self::BuiltinSimple(_) => {
                ret.insert("N".to_string(), Typ::Numeric);
                (ret, Typ::Numeric)
            },
            Self::Echo => {
                ret.insert("N".to_string(), Typ::Numeric);
                (ret, Typ::Null)
            },
            Self::User(_r, orig_args, (rettyp, _)) => {
                for (arg, typ) in orig_args {
                    ret.insert(arg.fragment().to_string(), typ.clone());
                }
                (ret, rettyp.clone())
            },
            Self::LoopPrimitive(_) => {
                (ret, Typ::Loop)
            },
            Self::LoopTransform(_) => {
                ret.insert("X".to_string(), Typ::Numeric);
                ret.insert("Y".to_string(), Typ::Numeric);
                ret.insert("GEO".to_string(), Typ::Loop);
                (ret, Typ::Loop)
            },
            Self::AreaPromotion => {
                ret.insert("GEO".to_string(), Typ::Loop);
                (ret, Typ::Area)
            },
            Self::AreaEncompass => {
                ret.insert("GEO".to_string(), Typ::Loop);
                ret.insert("IN".to_string(), Typ::Area);
                (ret, Typ::Area)
            },
            Self::FitLoop => {
                ret.insert("X".to_string(), Typ::Func);
                ret.insert("Y".to_string(), Typ::Func);
                (ret, Typ::Loop)
            },
        }
    }
}

fn interpret_region<'a>(r: &Region<'a>, env: &mut Environment<'a>)
    -> Result<Object<'a>, crate::NurbsimplError<'a>> {
    // put variables in resolver
    let mut to_resolve = HashMap::new();
    let mut to_call = vec![];
    let mut resolve_names = vec![];
    let mut variables = HashMap::new();
    let mut retstatement = None;
    for statement in &r.statements {
        match statement {
            Statement::VariableDecl(v) => {
                let name = v.name;
                to_resolve.insert(name.fragment().to_string(), 
                                  ResolvingProgress::Unresolved(v.clone()));
                resolve_names.push(name);
            },
            Statement::Call(c) => {
                to_call.push(c);
            },
            Statement::FunctionDecl(f) => {
                // make a func around this region
                // Convert args:
                let args: Result<HashMap<Span<'a>, Typ>, NurbsimplError<'a>> = 
                    f.args.iter().map(|(n, t)| Ok((n.clone(), t.to_typ()?))).collect();
                let o = Object::Func(
                    Callable::User(f.body.clone(), args?, (f.ret.to_typ()?, f.ret.sp))
                    );
                // put in variables
                variables.insert(f.name.fragment().to_string(), o);
            },
            Statement::Return(r) => {
                if let Some(r2) = retstatement {
                    return Err(NurbsimplError::MultipleReturn(r.clone(), r2));
                } else {
                    retstatement = Some(r.clone());
                }
            },
        }
    }
    let mut scope = NameScope {
        variables,
        to_resolve: to_resolve.clone(),
    };
    // Assign concrete values to all variables
    for var in resolve_names {
        let _res = resolve_variable(var.fragment().to_string(), env, &mut scope, &var);
    }
    env.scope_stack.push(scope);
    // Dummy scope, shouldn't actually be modified TODO codify this into type system
    let mut scope = NameScope {
        variables: HashMap::new(),
        to_resolve: HashMap::new(),
    };
    // Run all top-level functions (sshhhhhh yes we do just assume they all have important side
    // effects)
    for c in to_call {
        let _res = c.call(env, &mut scope)?;
    }
    // return if applicable
    let ret = if let Some(retexp) = retstatement {
        retexp.evaluate(env, &mut scope)
    } else {
        Ok(Object::Null)
    };
    env.scope_stack.pop();
    ret
}

fn resolve_variable<'a>(name: String, env: &mut Environment<'a>, scope: &mut NameScope<'a>, from: &Span<'a>)
-> Result<Object<'a>, NurbsimplError<'a>> {
    // Have we already resolved this variable?
    if let Some(v) = scope.to_resolve.remove(&name) {
        // we have not resolved it!
        if let ResolvingProgress::Unresolved(v) = v {
            // mark as in progress
            scope.to_resolve.insert(name.clone(), ResolvingProgress::Resolving(v.clone()));
            // go hunt for an actual value
            let res = v.resolve(env, scope);
            // mark as done
            scope.to_resolve.remove(&name);
            // wooooo!
            res
        } else if let ResolvingProgress::Resolving(v) = v {
            // otherwise it's in-progress and we have recursed (bad!)
            return Err(NurbsimplError::RecursiveDecl(name, v.clone()));
        } else {
            unreachable!();
        }
    } else {
        // it either has been resolved or it doesn't exist
        // check current scope's resolution
        if let Some(v) = scope.variables.get(&name) {
            return Ok((*v).clone());
        }
        // check previous fully-resolved scopes
        for scope in env.scope_stack.iter().rev() {
            if let Some(v) = scope.variables.get(&name) {
                return Ok((*v).clone());
            }
        }
        Err(NurbsimplError::NonexistentName(env.clone(), scope.clone(), name.clone(), from.clone()))
    }
}

/// Runs the program, panicing on error.
pub fn run<'a>(name: String, program: &'a str) -> Result<ObjectHandle<'a>, NurbsimplError<'a>>{
    trace!("Loaded program {}", name);
    let prog_ast = ast::parse_to_ast(program)?;
    trace!("Parsed program {} to AST {:?}", name, prog_ast);
    let mut env = Environment::basic();
    let res = interpret_region(&prog_ast, &mut env)?;
    trace!("Ran program {} and got {:?}", name, res);
    Ok(ObjectHandle(res))
}
