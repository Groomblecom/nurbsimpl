use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, one_of, multispace0, multispace1},
    combinator::complete,
    multi::{many1, separated_list0},
    sequence::{tuple, delimited},
    number::complete::float,
    error::{context, VerboseError, ParseError},
    IResult,
    Parser,
};

use nom_locate::LocatedSpan;
use nom_recursive::{recursive_parser, RecursiveInfo};

use crate::{Environment, NameScope, Object, NurbsimplError, resolve_variable, Typ};

// TODO: make everything track locate-spans for syntax highlighting goodness/better errors
pub(crate) type Span<'a> = LocatedSpan<&'a str, RecursiveInfo>;

use std::collections::HashMap;

type Res<T, U> = IResult<T, U, VerboseError<T>>;

// From the nom recipes:
/// A combinator that takes a parser `inner` and produces a parser that also consumes both leading and 
/// trailing whitespace, returning the output of `inner`.
fn ws<'a, F: 'a, O, E: ParseError<Span<'a>>>(inner: F) -> impl FnMut(Span<'a>) -> IResult<Span<'a>, O, E>
where
F: Fn(Span<'a>) -> IResult<Span<'a>, O, E>,
{
    delimited(
        multispace0,
        inner,
        multispace0
    )
}

#[derive(Debug, Clone)]
pub(crate) struct Region<'a> {
    pub(crate) statements: Vec<Statement<'a>>,
}

#[derive(Debug, Clone)]
pub enum Statement<'a> {
    Call(Call<'a>),
    VariableDecl(VariableDecl<'a>),
    Return(ReturnStatement<'a>),
    FunctionDecl(FunctionDecl<'a>),
}

#[derive(Debug, Clone)]
pub struct ReturnStatement<'a> {
    pub(crate) exp: Expression<'a>,
}

impl<'a> ReturnStatement<'a> {
    pub(crate) fn evaluate(&self, env: &mut Environment<'a>, scope: &mut NameScope<'a>) 
        -> Result<Object<'a>, NurbsimplError<'a>> {
            self.exp.evaluate(env, scope)
    }
}

#[derive(Debug, Clone)]
pub struct FunctionDecl<'a> {
    pub(crate) name: Span<'a>,
    pub(crate) body: Region<'a>,
    pub(crate) args: HashMap<Span<'a>, TypToken<'a>>,
    pub(crate) ret: TypToken<'a>,
}

#[derive(Debug, Clone)]
pub struct VariableDecl<'a> {
    pub(crate) name: Span<'a>,
    pub(crate) rhs: Expression<'a>,
}

impl<'a> VariableDecl<'a> {
    pub(crate) fn resolve(&self, env: &mut Environment<'a>, scope: &mut NameScope<'a>) 
        -> Result<Object<'a>, NurbsimplError<'a>> {
            let val = self.rhs.evaluate(env, scope)?;
            let res = scope.variables.insert(self.name.fragment().to_string(), val.clone());
            if res.is_some() {
                return Err(NurbsimplError::ReusedName(self.name.clone()));
            }
            Ok(val)
    }
}

#[derive(Debug, Clone)]
pub(crate) struct TypToken<'a> {
    pub(crate) sp: Span<'a>,
}

impl<'a> TypToken<'a> {
    pub(crate) fn to_typ(&self) -> Result<crate::Typ, NurbsimplError<'a>> {
        match self.sp.fragment().parse() {
            Ok(t) => Ok(t),
            Err(_e) => Err(crate::NurbsimplError::TypeMissing(self.sp.clone()))
        }
    }
}

#[derive(Debug, Clone)]
pub struct Call<'a> {
    name: Span<'a>,
    arguments: HashMap<Span<'a>, Expression<'a>>,
}

impl<'a> From<(Span<'a>, Span<'a>, Vec<(Span<'a>, Span<'a>, Expression<'a>)>, Span<'a>)> for Call<'a> {
    fn from(i: (Span<'a>, Span<'a>, Vec<(Span<'a>, Span<'a>, Expression<'a>)>, Span<'a>)) -> Call<'a> {
        let (name, _, pairs, _) = i;
        let mut arguments = HashMap::new();
        for pair in pairs {
            // always true
            assert_eq!(pair.1.fragment(), &"=");
            arguments.insert(pair.0, pair.2);
        }
        Call {
            name, arguments
        }
    }
}

#[derive(Debug, Clone)]
pub enum Expression<'a> {
    Numeric(NumericExpression<'a>),
    //Boolean(BoolExpression<'a>),
    //Ternary(BoolExpression<'a>, Box<(Expression<'a>, Expression<'a>)>),
    Call(Call<'a>),
    Variable(Span<'a>),
}

#[derive(Debug, Clone)]
pub enum NumericExpression<'a> {
    Unary(char, Box<NumericExpression<'a>>),
    Additive(char, Box<(NumericExpression<'a>, NumericExpression<'a>)>),
    Multiplicative(char, Box<(NumericExpression<'a>, NumericExpression<'a>)>),
    //Ternary(Box<(BoolExpression<'a>, NumericExpression<'a>, NumericExpression<'a>)>),
    Literal(f32),
    Parenthetical(Box<NumericExpression<'a>>),
    Variable(Span<'a>),
}

/*
#[derive(Debug, Clone)]
pub enum BoolExpression<'a> {
    Unary(Span<'a>, Box<BoolExpression<'a>>),
    Binary(Span<'a>, Box<(BoolExpression<'a>, BoolExpression<'a>)>),
    Parenthetical(Box<BoolExpression<'a>>),
    Conditional(Span<'a>, Box<(NumericExpression<'a>, NumericExpression<'a>)>),
    Literal(bool),
    Variable(Span<'a>),
}
*/

// EVALUATION STUFF HERE
//
// keep it as separate as possible from later grammar rules
// TODO make this two files or sth
impl<'a> Expression<'a> {
    fn evaluate(&self, env: &mut Environment<'a>, scope: &mut NameScope<'a>)
        -> Result<Object<'a>, NurbsimplError<'a>> {
        match self {
            Self::Numeric(e) => Ok(Object::Numeric(e.evaluate(env, scope)?)),
            Self::Call(c) => c.call(env, scope),
            Self::Variable(v) => resolve_variable(v.fragment().to_string(), env, scope, v)
        }
    }
}
impl<'a> NumericExpression<'a> {
    fn evaluate(&self, env: &mut Environment<'a>, scope: &mut NameScope<'a>)
        -> Result<f32, NurbsimplError<'a>> {
        match self {
            Self::Parenthetical(exp) => {
                exp.evaluate(env, scope)
            },
            Self::Unary(c, exp) => {
                match c {
                    '-' => Ok(-exp.evaluate(env, scope)?),
                    _ => unimplemented!(),
                }
            },
            Self::Additive(c, arg) => {
                let a = (*arg).0.evaluate(env, scope)?;
                let b = (*arg).1.evaluate(env, scope)?;
                match c {
                    '-' => Ok(a-b),
                    '+' => Ok(a+b),
                    _ => unimplemented!(),
                }
            },
            Self::Multiplicative(c, arg) => {
                let a = (*arg).0.evaluate(env, scope)?;
                let b = (*arg).1.evaluate(env, scope)?;
                match c {
                    '*' => Ok(a*b),
                    '/' => Ok(a/b),
                    _ => unimplemented!(),
                }
            },
            Self::Literal(l) => Ok(*l),
            Self::Variable(v) => {
                let res = resolve_variable(v.fragment().to_string(), env, scope, v)?;
                // this object had better be a number
                if let Object::Numeric(n) = res {
                    Ok(n)
                } else {
                    Err(NurbsimplError::TypeMismatch(Typ::Numeric, res.typ(), *v))
                }
            },
        }
    }
}

impl<'a> Call<'a> {
    pub(crate) fn call(&self, env: &mut Environment<'a>, scope: &mut NameScope<'a>)
        -> Result<Object<'a>, NurbsimplError<'a>> {
            // get the object corresponding to this name
            let c = resolve_variable(self.name.fragment().to_string(), env, scope, &self.name)?;
            let c = if let Object::Func(c) = c {
                c
            } else {
                return Err(NurbsimplError::NotCallable(self.name.clone()));
            };
            // get the argument objects
            let mut argmap = HashMap::new();
            for (name, arg) in self.arguments.iter() {
                argmap.insert(name.fragment().to_string(), arg.evaluate(env, scope)?);
            }
            // call the underlying callable
            c.call(argmap, env)
        }
}

// ###################
//
// GRAMMAR RULES AHEAD
//
// ###################

fn region<'a>(input: Span<'a>) -> Res<Span, Region<'a>> {
    context(
        "region",
        many1(statement)
        )(input)
        .map(|(con, statements)| (con, Region {
            statements: statements,
        }))
}
fn statement<'a>(input: Span<'a>) -> Res<Span, Statement<'a>> {
    context(
        "statement",
        alt((ws(variable_decl), ws(function_decl), ws(call_statement), ws(return_statement)))
        )(input)
        .map(|(con, c)| (con, c))
}

#[recursive_parser]
fn call<'a>(s: Span<'a>) -> Res<Span<'a>, Call<'a>> {
    let (s, v) = ws(alpha1)(s)?;
    let (s, x) = ws(tag("("))(s)?;
    let (s, y) = separated_list0(tag(","), tuple((alpha1, tag("="), expression)))(s)?;
    let (s, z) = ws(tag(")"))(s)?;
    let ret = (v, x, y, z).into();
    Ok((s, ret))
}
fn return_statement<'a>(input: Span<'a>) -> Res<Span, Statement<'a>> {
    context(
        "variable declaration",
        tuple((tag("return"), multispace1, expression, ws(tag(";")))), 
        )(input)
        .map(|(con, c)| {
            (con, Statement::Return(ReturnStatement {
                exp: c.2,
            }))
        })
}
fn variable_decl<'a>(input: Span<'a>) -> Res<Span, Statement<'a>> {
    context(
        "variable declaration",
        tuple((tag("let"), multispace1, alpha1, ws(tag("=")), expression, ws(tag(";")))), 
        )(input)
        .map(|(con, c)| {
            (con, Statement::VariableDecl(VariableDecl {
                name: c.2,
                rhs: c.4,
            }))
        })
}
fn function_decl<'a>(input: Span<'a>) -> Res<Span, Statement<'a>> {
    context(
        "variable declaration",
        tuple((tag("function"), multispace1, alpha1, 
               ws(tag("(")),
               separated_list0(ws(tag(",")), tuple((alpha1, ws(tag(":")), typ))),
               tag(")"), ws(tag("->")),
               typ,
               ws(tag("{")), region, ws(tag("}")))), 
        )(input)
        .map(|(con, c)| {
            (con, Statement::FunctionDecl(FunctionDecl {
                name: c.2,
                args: to_map_of_typ(c.4),
                body: c.9,
                ret: c.7,
            }))
        })
}

fn to_map_of_typ<'a>(pairs: Vec<(Span<'a>, Span<'a>, TypToken<'a>)>) -> HashMap<Span<'a>, TypToken<'a>> {
        let mut arguments = HashMap::new();
        for pair in pairs {
            // always true
            assert_eq!(pair.1.fragment(), &":");
            arguments.insert(pair.0, pair.2);
        }
        arguments
}

fn typ<'a>(input: Span<'a>) -> Res<Span<'a>, TypToken> {
    // There has to be a prettier way to do this
    context(
        "Type name",
        alt((tag("Numeric"), tag("Func"), tag("Null"))),
        )(input)
        .map(|(con, c)| {
            (con, TypToken{sp: c})
        })
}

fn expression<'a>(input: Span<'a>) -> Res<Span, Expression<'a>> {
    let f = context(
        "object expression",
        alt((
                exp_call,
                numeric.map(|c| Expression::Numeric(c)),
                exp_variable,
                )
            ) //, exp_boolean, ternary, exp_call
        )(input)
        .map(|(con, c)| (con, c))
        ;
    f
}

fn exp_call<'a>(input: Span<'a>) -> Res<Span, Expression<'a>> {
    context(
        "Function call as expression",
        ws(call)
        )(input)
        .map(|(con, c)| (con, Expression::Call(c)))
}
fn call_statement<'a>(input: Span<'a>) -> Res<Span, Statement<'a>> {
    context(
        "Call as statement",
        tuple((ws(call), tag(";")))
        )(input)
        .map(|(con, c)| (con, Statement::Call(c.0)))
}
 
fn exp_variable<'a>(input: Span<'a>) -> Res<Span, Expression<'a>> {
    context(
        "Funky Variable",
        ws(alpha1) //, exp_boolean, ternary, exp_call, exp_variable
        )(input)
        .map(|(con, c)| (con, Expression::Variable(c)))
}
// Only use this one in contexts where variables MUST be numeric-typed; e.g. subexpressions of
// numeric operators.
fn numeric_w_var<'a>(input: Span<'a>) -> Res<Span, NumericExpression<'a>> {
    context(
        "Numeric Expression",
        alt((num_parenthetical,multiplicative,additive,unary,num_literal,num_variable,)) //TODO ternary
        )(input)
        .map(|(con, c)| (con, c))
}
// Use this one everywhere else.
fn numeric<'a>(input: Span<'a>) -> Res<Span, NumericExpression<'a>> {
    context(
        "Numeric Expression",
        alt((num_parenthetical,multiplicative,additive,unary,num_literal,)) //TODO ternary
        )(input)
        .map(|(con, c)| (con, c))
}
#[recursive_parser]
fn additive<'a>(s: Span<'a>) -> Res<Span<'a>, NumericExpression<'a>> {
    let (s, x) = numeric_w_var(s)?;
    let (s, y) = ws(one_of("+-"))(s)?;
    let (s, z) = numeric_w_var(s)?;
    let ret = NumericExpression::Additive(y, Box::new((x, z)));
    Ok((s, ret))
}
#[recursive_parser]
fn multiplicative<'a>(s: Span<'a>) -> Res<Span<'a>, NumericExpression<'a>> {
    let (s, x) = numeric_w_var(s)?;
    let (s, y) = ws(one_of("*/"))(s)?;
    let (s, z) = numeric_w_var(s)?;
    let ret = NumericExpression::Multiplicative(y, Box::new((x, z)));
    Ok((s, ret))
}
#[recursive_parser]
fn num_parenthetical<'a>(s: Span<'a>) -> Res<Span<'a>, NumericExpression<'a>> {
    let (s, _x) = ws(tag("("))(s)?;
    let (s, y) = numeric_w_var(s)?;
    let (s, _z) = ws(tag(")"))(s)?;
    let ret = NumericExpression::Parenthetical(Box::new(y));
    Ok((s, ret))
}
// TODO: fix broken operator precedence!
#[recursive_parser]
fn unary<'a>(s: Span<'a>) -> Res<Span<'a>, NumericExpression<'a>> {
    let (s, x) = ws(one_of("-"))(s)?;
    let (s, y) = numeric_w_var(s)?;
    let ret = NumericExpression::Unary(x, Box::new(y));
    Ok((s, ret))
}
fn num_literal<'a>(input: Span<'a>) -> Res<Span<'a>, NumericExpression<'a>> {
    context(
        "Numeric literal",
        ws(float)
        )(input)
        .map(|(con, c)| (con, NumericExpression::Literal(c)))
}
fn num_variable<'a>(input: Span<'a>) -> Res<Span, NumericExpression<'a>> {
    context(
        "Funky Variable",
        ws(alpha1) //, exp_boolean, ternary, exp_call, exp_variable
        )(input)
        .map(|(con, c)| (con, NumericExpression::Variable(c)))
}

pub(crate) fn parse_to_ast(program: &str) -> Result<Region, crate::NurbsimplError> {
    let res = complete(region)(LocatedSpan::new_extra(program, RecursiveInfo::new()))?;
    log::trace!("res: {:#?}", res);
    if res.0.fragment().len() > 0 {
        return Err(NurbsimplError::UnableToFinish(res.0));
    }
    Ok(res.1)
}

#[cfg(test)]
mod tests {
    use crate::ast::*;
    #[test]
    fn it_works() {
        let program = r#"
        let varname = 3+2;
        func(foo=bar,bar=-3 + 5);
        "#;
        let res = region(LocatedSpan::new_extra(program, RecursiveInfo::new()));
        println!("{:?}", res);
    }
}
