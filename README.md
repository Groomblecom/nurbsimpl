NURBSIMPL
---

(name provisional)

`nurbsimpl` contains the library itself. Run `cargo test` in that directory to
test your changes. `nurbsimpl-demo` holds demo and additional test code for the
library (presently all 2D plotting demos). To generate these demos, run
`cargo run` in that directory, and PNGs will be produced in an `output`
subdirectory. `pitch slides` contains the original project proposal slides.
